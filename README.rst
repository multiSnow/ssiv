ssiv
====

Dependence:
 - | `Python`_ >= 3.11
 - | `PySDL2`_ >= 0.9.15,
   | with `SDL2`_ library >= 2.26.0
 - | `Wand`_ >= 0.6.11,
   | with `MagickWand`_ library >= 6.9.0

Optional:
 - | `chardet`_ >= 5.0.0
   | (to detect file names encoding in legacy format ZIP archive)
 - | `psutil`_ >= 5.9.4
   | (to show memory usage in "pref" log level)

Supported platform:
 - linux
 - win32
 - (others need to test: such as cygwin, darwin, bsd, ...)

Feature:
 - | configurable full keyboard control
 - | decode image(s) in thread
 - | show multi-frame APNG with alpha channel correctly
   | (compared with `animate`)
 - | show the "canvas" of image
   | (the "Page geometry" of `identify -verbose`)
 - | preview multiple images in one screen
   | (the "thumbnail mode" of sxiv/nsxiv)
 - | show two portrait page in one screen
   | (the "double page mode" and "manga mode" of comix/mcomix)
 - | open zip archive in-place as a folder

Defect:
 - | possibly high memory usage
   | (twice of full pixels of all decoding images in thread as a peak usage)
   | (could be mitigated by setting 'wand_loader.maxsize'
     and lowering 'wand_loader.procs')
 - | possibly memory leak caused by overload in VRAM if using hardware renderer
   | (cache in VRAM by default which could not trigger Out-of-Memory event)
   | (could be mitigated by setting 'renderer.driver' to 'software'
     and lowering 'user_interface.cache')
 - | multi-frame image will be downscaled if using hardware renderer
   | (according to the maximum texture size)
 - | encrypted contents in zip archive are not supported

Usage:
 - get source tree::

    $ git clone https://git.disroot.org/multiSnow/ssiv.git <destination>
    $ cd <destination>

 - run from source tree::

    $ python3 src [options, ...]

   or, copy and run from anywhere::

    $ cp -r src <target_directory>/<new_name>
    $ python3 <target_directory>/<new_name> [options, ...]

 - create and run pyz archive

   (which will use system-installed PySDL2 and Wand)::

    $ python3 -m zipapp src --output=<new_name>.pyz --compress
    $ python3 <new_name>.pyz [options, ...]

   or, create and run pyz archive with bundling PySDL2 and Wand

   (which still need to install shared library of SDL2 and MagickWand)::

    $ sh mkpyz.sh
    $ python3 ssiv.pyz [options, ...]

 - install as library module using setup.py and run as a script::

    $ python3 setup.py install [other setuptools options, ...]
    $ python3 -m ssiv [option, ...]

Commandline:
 ::

   ssiv [-h] [-v]
   ssiv [-d CONFIG]
   ssiv [-f CONFIG] [IMAGE ...] -c [CONTAINER ...]

-h, --help                show help message and exit
-v, --version             show version and exit
-f CONFIG, --conf CONFIG  load custom config at CONFIG, - for STDIN
-d CONFIG, --dump CONFIG  dump default config to CONFIG, - for STDOUT

.. _Python: https://www.python.org/
.. _PySDL2: https://github.com/py-sdl/py-sdl2
.. _SDL2: https://www.libsdl.org/
.. _Wand: https://docs.wand-py.org/
.. _MagickWand: https://imagemagick.org/
.. _chardet: https://github.com/chardet/chardet
.. _psutil: https://github.com/giampaolo/psutil
