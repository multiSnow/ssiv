#!/bin/sh

#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

set -e

ROOT=src
NAME=ssiv

if ! test -d ${ROOT}/sdl2; then
    if ! test -d py-sdl2/sdl2; then
        git clone --depth=1 https://github.com/py-sdl/py-sdl2.git
    fi
    # only the 'core' files of PySDL2 is needed
    # only the 'core' library of SDL2 is needed
    mkdir -p ${ROOT}/sdl2
    for path in py-sdl2/sdl2/*.py; do
        case `basename ${path}` in
            sdl*.py) ;;
            *.py) cp ${path} ${ROOT}/sdl2 ;;
        esac
    done
fi

if ! test -d ${ROOT}/wand; then
    if ! test -d wand/wand; then
        git clone --depth=1 https://github.com/emcconville/wand.git
    fi
    cp -r wand/wand ${ROOT}
fi

# prefer 7z for higher compression ratio

for prog in 7za 7z; do
    if which ${prog}; then
        PROG=${prog}
        break
    fi
done

if test x${PROG} != x; then
    cd ${ROOT}
    ${PROG} a -tzip -mm=Deflate -mx9 -mfb258 -mpass=15 ../${NAME}.pyz
    exit $?
fi

for prog in python3 python; do
    if which ${prog}; then
        PROG=${prog}
        break
    fi
done

if test x${PROG} != x; then
    ${PROG} -m zipapp ${ROOT} --output=${NAME}.pyz --compress
fi
