#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys

curdir=os.path.dirname(__file__)

sys.path.insert(0,os.path.join(curdir,'src'))
from ssiv.constants import *

with open(os.path.join(curdir,'setup.cfg.in'),mode='rt') as fi,\
     open(os.path.join(curdir,'setup.cfg'),mode='wt') as fo:
    fo.write(fi.read().format(
        name=progname.lower(),
        version='{}.{}.{}'.format(*version_info),
        pysdl2ver='{}.{}.{}'.format(*required_pysdl2ver),
        wandver='{}.{}.{}'.format(*required_wandver),
    ))

if not sys.argv[1:]:
    print('generate setup.cfg only.')
    print(f'see {sys.argv[0]} --help for setuptoools command')
    exit()

from setuptools import setup
setup()


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
