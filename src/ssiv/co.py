#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from collections.abc import Mapping
from sys import getsizeof as _sizeof

__all__=['OrderedDict','AttrDict']

class AttrDict:
    __slots__='_dict',

    def __init__(self,*args,**kwds):
        self._dict=dict(*args,**kwds)

    def __repr__(self):
        inner=', '.join(f'{k}={v!r}' for k,v in self._dict.items())
        return f'{self.__class__.__name__}({{{inner}}})'

    def __str__(self):
        return repr(self)

    def __getstate__(self):
        return [(k,v) for k,v in self._dict.items()]

    def __setstate__(self,state):
        self._dict=dict(state)

    def __getitem__(self,key):
        return self._dict.__getitem__(key)

    def __setitem__(self,key,value):
        self._dict.__setitem__(key,value)

    def __delitem__(self,key):
        self._dict.__delitem__(key)

    def __iter__(self):
        return self._dict.__iter__()

    def __getattr__(self,attr):
        if attr.startswith('_'):
            return super().__getattr__(attr)
        else:
            try:
                return self._dict.__getitem__(attr)
            except KeyError:
                pass
        raise AttributeError(attr)

    def __setattr__(self,attr,value):
        if attr.startswith('_'):
            super().__setattr__(attr,value)
        else:
            self._dict.__setitem__(attr,value)

    def __delattr__(self,attr):
        if attr.startswith('_'):
            super().__delattr__(attr)
        else:
            try:
                return self._dict.__delitem__(attr)
            except KeyError:
                pass
        raise AttributeError(attr)

    def __or__(self,other):
        assert isinstance(other,self.__class__)
        new=self.__class__(self._dict)
        new|=other
        return new

    def __ior__(self,other):
        assert isinstance(other,self.__class__)
        self._dict.update(other._dict)
        return self

    def __sizeof__(self):
        return _sizeof(self._dict)

class OrderedDict(Mapping):
    __slots__='_dict','_keys'

    def __new__(cls,*args,**kwds):
        instance=super().__new__(cls)
        instance._dict={}
        instance._keys=[]
        return instance

    def __init__(self,other=None,/,**kwds):
        initdata={}
        if other is not None:
            if isinstance(other,OrderedDict):
                for key in other:
                    initdata[key]=other[key]
            else:
                initdata|=other
        initdata|=kwds
        for k,v in initdata.items():
            self.__setitem__(k,v)

    def __repr__(self):
        inner=', '.join(f'{k!r}: {v!r}' for k,v in self.items())
        return f'{self.__class__.__name__}({{{inner}}})'

    def __str__(self):
        return repr(self)

    def __bool__(self):
        return not not self._keys

    def __len__(self):
        return self._keys.__len__()

    def __getitem__(self,key):
        return self._dict.__getitem__(key)

    def __setitem__(self,key,value):
        if key not in self._keys:
            self._keys.append(key)
        self._dict.__setitem__(key,value)

    def __delitem__(self,key):
        self._dict.__delitem__(key)
        self._keys.remove(key)

    def __iter__(self):
        return iter(self._keys)

    def __reversed__(self):
        return reversed(self._keys)

    def __contains__(self,key):
        return key in self._keys

    def __or__(self,other):
        assert isinstance(other,self.__class__)
        new=self.__class__(self)
        new|=other
        return new

    def __ior__(self,other):
        assert isinstance(other,self.__class__)
        other=self.__class__(other)
        for key,value in other.items():
            self.__setitem__(key,value)
        return self

    def __sizeof__(self):
        return _sizeof(self._keys)+_sizeof(self._dict)

    @classmethod
    def fromkeys(cls,iterable,value=None):
        return cls((key,value) for key in iterable)

    def copy(self):
        new=self.__class__()
        new|=self
        return new

    def clear(self):
        self._keys.clear()
        self._dict.clear()

    def getkey(self,pos=-1,/):
        return self._keys[pos]

    def getvalue(self,pos=-1,/):
        return self.__getitem__(self.getkey(pos))

    def getitem(self,pos=-1,/):
        return self.getkey(pos),self.getvalue(pos)

    def pop(self,key,default=Exception,/):
        if key not in self:
            if default is Exception:
                raise KeyError(key)
            return default
        self._keys.remove(key)
        return self._dict.pop(key)

    def popitem(self,pos=-1,/):
        return (key:=self._keys.pop(pos)),self._dict.pop(key)

    def setdefault(self,key,default=None,/):
        if key not in self:
            self.__setitem__(key,default)
        return self.__getitem__(key)

    def update(self,other=None,/,**kwds):
        other=self.__class__(other,**kwds)
        self|=other

    def move_to_end(self,key,last=True):
        self._keys.remove(key)
        if last:
            self._keys.append(key)
        else:
            self._keys.insert(0,key)

    def sort(self,*,key=None,reverse=False):
        self._keys.sort(key=key,reverse=reverse)

    def reverse(self):
        self._keys.reverse()

    def index(self,key):
        return self._keys.index(key)

    def insert(self,pos,key,value=None,/):
        self.__setitem__(key,value)
        self._keys.remove(key)
        self._keys.insert(pos,key)


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
