#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from ..constants import progname

default=f'''# config file is in toml format.
# see https://toml.io/ for full spec

# program config

[file_reader]
# keepalive of file_reader process, in seconds
keepalive = 5
# whether to access directory recursively
recursive = false
# whether to follow symbol link
follow = true

[wand_loader]
# amount of concurrently decoding process, 0 to auto detect
procs = 0
# downscale to {{maxsize}}x{{maxsize}}, 0 to disable downscale
maxsize = 0
# whether to load multi frame image
anime = true
# whether to load apng as multi frame image, no effect if {{anime}} is false
apng = true

[user_interface]
# amount of decoded image to keep as cache, 0 to disable cache
cache = 0
# whether using numeric sort (('a1', 'a10', 'a2') => ('a1', 'a2', 'a10'))
numsort = false
# start with showing page
usepage = false
# start with showing double image in canvas mode
double = false
# start with rtl mode
# (first in right of double canvas mode, start from right-top in thumbnail mode)
rtl = false
# start with anti-alias enabled
antialias = false
# start with thumbnail mode
tiling = false
# multiplier for fast move, at lease 2 is required
faststep = 20
# whether to enable screensaver while {progname.lower()} is running
screensaver = true
# whether to start in fullscreen mode
startfs = false
# whether to change videomode in fullscreen mode
realfs = false

[thumbnail]
# thumbnail size as {{size}}x{{size}}
size = 64
# max thumbnail size as {{size}}x{{size}}
maxsize = 256
# gap between thumbnails
gap = 8

[style]
# window background color, [r, g, b], each value from 0 to 255
bgcolor = [0, 0, 0]
# base color for selector and indicator, [r, g, b], each value from 0 to 255
fgcolor = [200, 200, 200]
# default window size, [width, height]
size = [800, 600]
# whether to draw checker for alpha and page from start
checker = true
# whether to show window decoration
decoration = true

[statusbar]
# size of statusbar, at least 12 is required, or 0 to permanently disable
size = 16
# statusbar position in landscape window, ('top', 'bottom', 'left', 'right')
posland = 'right'
# statusbar position in portrait window, ('top', 'bottom', 'left', 'right')
posport = 'top'

[motionbar]
# size of motion indicator, at last 12 is required
size = 16
# motion indicator for x-axis, ('top', 'bottom', 'disabled')
posxaxis = 'bottom'
# motion indicator for y-axis, ('left', 'right', 'disabled')
posyaxis = 'left'

[renderer]
# renderer driver to use. false, empty string or numeric 0 to auto detect
driver = 0
# whether to enable vsync. vsync is always enabled in software renderer
vsync = false
# whether to set DRI_PRIME environment
prime = false

[log]
# log level, bitwise OR:
#   1, fault
#   2, error
#   4, warn
#   8, note
#  16, info
#  32, verb
#  64, trace
# 128, pref
#
# overrided by setting the {progname}_LOG_LEVEL evironment variable
# completely disable log by passing the -O/-OO command-line option
# or setting the PYTHONOPTIMIZE evironment variable
level = 15

# whether to enable traceback in exception log
# overrided by setting the {progname}_LOG_DEVEL evironment variable
devel = false

# filename of stdout/stderr
# empty string to use system stdout/stderr
# false to disable output
# overrided by the {progname}_LOG_STDOUT/{progname}_LOG_STDERR environment variable
stdout = ''
stderr = ''

# whether to use color in log ('always', 'never', 'auto')
# overrided by setting the {progname}_LOG_COLOR evironment variable
color = 'auto'

[environ]
# *** environment variables set here will override that set by system ***
# value of each option is a list contains lists with two items as name and value
# empty value means no environment variable will set
# both name and value should be string, case-sensitive

# environment variables for ImageMagick to be set as:
#
#  MAGICK_{{name}}={{value}}
#
# e.g
#  ['MEMORY_LIMIT', '100MB'] => MAGICK_MEMORY_LIMIT=100MB
#
# see https://www.imagemagick.org/script/resources.php#environment
# for environment variables recognized by ImageMagick
magick = []

# environment variables for SDL to be set as:
#
#  SDL_{{name}}={{value}}
#
# e.g
#  ['VIDEO_X11_WMCLASS', 'SSIV'] => SDL_VIDEO_X11_WMCLASS=SSIV
#
sdl = []


# keybinding config

# value of each action is a list contains list of SDL_Keycode, order does not matter
# action will be toggled only if pressed keys are full matched to the list of SDL_Keycode
# motion keybindings act only in canvas mode, others act when key released
# no further action toggled until new key pressed or all keys released
#
# list of SDL_Keycode should not duplicate within section
# list of SDL_Keycode will be ignored if duplicate in kbd_global section
# list of SDL_Keycode in kbd_motion should not duplicate with kbd_canvas
#
# see https://wiki.libsdl.org/SDL_Keycode for SDL_Keycode value
# enable 'trace' log level to show SDL_Keycode value
#
# left and right ctrl/alt/shift could be matched separately
# or combined as SDLK_CTRL/SDLK_ALT/SDLK_SHIFT
# spearated ctrl/alt/shift will be matched before combined ctrl/alt/shift

# global keybindings, effect on all mode
[kbd_global]
# exit program
quit = [['SDLK_q'],['SDLK_CTRL','SDLK_q']]
# release cache
clear = []
# toggle rtl mode
rtl = []
# toggle fullscreen
fullscreen = [['SDLK_f']]

# enter tiling mode
tiling = [['SDLK_t']]
# enter canvas mode
canvas = [['SDLK_r']]

# toggle checker for alpha and page
checker = []
# toggle statusbar
showsb = []
# rotate statusbar
rotesb = []

# cross to next container
crossnext = []
# cross to previous container
crossprev = []

# keybindings that only effect on tiling mode
[kbd_tiling]
# move selector to one up
selectup = [['SDLK_UP']]
# move selector to one down
selectdown = [['SDLK_DOWN']]
# move selector to one left
selectleft = [['SDLK_LEFT']]
# move selector to one right
selectright = [['SDLK_RIGHT']]
# select first page
selectfirst = [['SDLK_a']]
# select last page
selectlast = [['SDLK_e']]
# scale twice up
scaleup = [['SDLK_SHIFT','SDLK_EQUALS']]
# scale harf down
scaledown = [['SDLK_MINUS']]

# keybindings that only effect on canvas mode
[kbd_canvas]
# toggle antialias
antialias = [['SDLK_SHIFT','SDLK_a']]
# toggle showing page
usepage = [['SDLK_p']]
# toggle showing double image
double = [['SDLK_d']]

# scale twice up, start from 2x
scaleup = [['SDLK_SHIFT','SDLK_EQUALS']]
# scale harf down, start from 0.5x
scaledown = [['SDLK_MINUS']]
# autofit
autofit = [['SDLK_EQUALS']]

# flip to next one or two page
flipnext = [['SDLK_PAGEDOWN']]
# flip to previous one or two page
flipprev = [['SDLK_PAGEUP']]
# flip to next one page
flipnextharf = [['SDLK_PAGEDOWN','SDLK_ALT']]
# flip to previous one page
flipprevharf = [['SDLK_PAGEUP','SDLK_ALT']]
# flip to first page
flipfirst = [['SDLK_a']]
# flip to last page
fliplast = [['SDLK_e']]

# keybindings act as motion that only effect on canvas mode
[kbd_motion]

# move view up
viewup = [['SDLK_UP']]
# move view down
viewdown = [['SDLK_DOWN']]
# move view left
viewleft = [['SDLK_LEFT']]
# move view right
viewright = [['SDLK_RIGHT']]

# move view left up
viewlu = [['SDLK_LEFT','SDLK_UP']]
# move view right up
viewru = [['SDLK_RIGHT','SDLK_UP']]
# move view left down
viewld = [['SDLK_LEFT','SDLK_DOWN']]
# move view right down
viewrd = [['SDLK_RIGHT','SDLK_DOWN']]

# move view up
viewfastup = [['SDLK_SHIFT','SDLK_UP']]
# move view down
viewfastdown = [['SDLK_SHIFT','SDLK_DOWN']]
# move view left
viewfastleft = [['SDLK_SHIFT','SDLK_LEFT']]
# move view right
viewfastright = [['SDLK_SHIFT','SDLK_RIGHT']]

# move view left up
viewfastlu = [['SDLK_SHIFT','SDLK_LEFT','SDLK_UP']]
# move view right up
viewfastru = [['SDLK_SHIFT','SDLK_RIGHT','SDLK_UP']]
# move view left down
viewfastld = [['SDLK_SHIFT','SDLK_LEFT','SDLK_DOWN']]
# move view right down
viewfastrd = [['SDLK_SHIFT','SDLK_RIGHT','SDLK_DOWN']]

# mouse control config

# mouse click control only occurred in canvase mode
# value of each action is a list of SDL_BUTTON
# order does not matter, amount of clicks does not matter
# action will be toggled only if no more button is pressed
#
# SDL_BUTTON should not duplicate within section
#
# see https://wiki.libsdl.org/SDL_MouseButtonEvent#remarks for SDL_BUTTON value
# enable 'trace' log level to show SDL_BUTTON value (nonstandard button included)
[mouse_clck]
# flip to next
flipnext = []
# flip to previous
flipprev = []
# cross to next container
crossnext = ['SDL_BUTTON_X2']
# cross to previous container
crossprev = ['SDL_BUTTON_X1']
# click left or right to flip according to rtl mode
clickflip = ['SDL_BUTTON_LEFT']


# mouse motion control only occurred in canvase mode
# value of each action is a list of list contains
# only one SDL_BUTTON with clicks and optional one or more SDL_Keycode
# order does not matter
# action will be toggled only if pressed button/key is full matched
#
# [SDL_BUTTON, clicks] should not duplicate within section
# SDL_Keycode parts should not duplicate with kbd_* section
#
# see https://wiki.libsdl.org/SDL_MouseButtonEvent#remarks for SDL_BUTTON value
# enable 'trace' log level to show SDL_BUTTON value
[mouse_mask]
# move view according to the relative motion
mtnpath = [['SDL_BUTTON_RIGHT',1]]
# move view according to the direction and distance
# related to the position of button clicked
mtnvector = [['SDL_BUTTON_RIGHT',2,'SDLK_CTRL']]

# mouse motion control only occurred in canvase mode
# value of each action is a list contains list of SDL_BUTTON/SDL_Keycode
# order does not matter
#
# list of SDL_Keycode should not duplicate with kbd_* section
#
# wheel event without mask button/key will act as moveview
# (same as viewup/viewdown/viewleft/viewright)
[wheel_mask]
# mask for scale
# (x+y, pulse for scale up, minus for scale down)
whlscale = [['SDLK_CTRL']]
# mask for fast move
whlfastmove = [['SDLK_LSHIFT']]

# direction of mouse wheel and motion
[mouse_dire]
# whether to reverse horizontal motion event
revxmtn = false
# whether to reverse vertical motion event
revymtn = false
# whether to reverse horizontal wheel event
revxwhl = false
# whether to reverse vertical wheel event
revywhl = false
'''

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
