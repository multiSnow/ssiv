#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

# this module should import nothing
# this module is safe to be imported with '*'

progname='SSIV'

version_info=(0,0,0)

# for tomllib, metadata_encoding keyword argument in zipfile.ZipFile
# and starred unpacking expressions in for-in statements
required_pyver=(3,11,0)

# TODO: test on different platform
supported_platform=('linux','win32')

# for SDL 2.26.0
required_pysdl2ver=(0,9,15)

# for SDL_GetWindowSizeInPixels
required_sdl2ver=(2,26,0)

# for fix https://github.com/emcconville/wand/issues/594
required_wandver=(0,6,11)

# supported MagickWand library version
# TODO: at least 7.0.0 required?
required_magickver=(6,9,0)

# exitcode for wand loader process
(
    WAND_SUCCESS,            # wand loader success exit (False-value)
    WAND_ARGUMENTS_INVALID,  # wand loader fault with invalid argument
    WAND_PARTIAL_SEND,       # wand loader exit before data sent completely
    WAND_UNSUPPORTED_FILE,   # wand loader failt with unsupported file or data
    *_,WAND_UNDEFINED_FAULT,
)=(0,*range(3,126))

# exitcode for file reader process
(
    FRDR_SUCCESS,            # file reader success exit (False-value)
    *_,FRDR_UNDEFINED_FAULT,
)=(0,*range(3,126))

# exitcode for ui process
(
    UI_SUCCESS,              # sdl interface success exit (False-value)
    *_,UI_UNDEFINED_FAULT,
)=(0,*range(3,126))

class ddict(dict):
    def __getitem__(self,key):
        try:
            return super().__getitem__(key)
        except:
            return key

WAND_RCNAME=ddict((globals()[s],s) for s in (
    'WAND_SUCCESS',
    'WAND_ARGUMENTS_INVALID',
    'WAND_PARTIAL_SEND',
    'WAND_UNSUPPORTED_FILE',
    'WAND_UNDEFINED_FAULT',
))

FRDR_RCNAME=ddict((globals()[s],s) for s in (
    'FRDR_SUCCESS',
    'FRDR_UNDEFINED_FAULT',
))

UI_RCNAME=ddict((globals()[s],s) for s in (
    'UI_SUCCESS',
    'UI_UNDEFINED_FAULT',
))

DISPOSE_NONE=0       # equivalent of APNG_DISPOSE_OP_NONE and NoneDispose
DISPOSE_BACKGROUND=1 # equivalent of APNG_DISPOSE_OP_BACKGROUND and BackgroundDispose
DISPOSE_PREVIOUS=2   # equivalent of APNG_DISPOSE_OP_PREVIOUS and PreviousDispose

BLEND_NONE=0         # equivalent of APNG_BLEND_OP_SOURCE and SDL_BLENDMODE_NONE
BLEND_BLEND=1        # equivalent of APNG_BLEND_OP_OVER and SDL_BLENDMODE_BLEND

__all__=(
    'WAND_SUCCESS','WAND_ARGUMENTS_INVALID','WAND_PARTIAL_SEND',
    'WAND_UNSUPPORTED_FILE','WAND_UNDEFINED_FAULT',
    'FRDR_SUCCESS','FRDR_UNDEFINED_FAULT','UI_SUCCESS','UI_UNDEFINED_FAULT',
    'WAND_RCNAME','FRDR_RCNAME','UI_RCNAME',
    'DISPOSE_NONE','DISPOSE_BACKGROUND','DISPOSE_PREVIOUS',
    'BLEND_NONE','BLEND_BLEND',
    'progname','version_info','required_pyver','supported_platform',
    'required_pysdl2ver','required_sdl2ver','required_wandver','required_magickver',
)


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
