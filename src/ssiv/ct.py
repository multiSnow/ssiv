#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

import ctypes
import ctypes.util as util
import os
import sys

def bytearray2void(bytearray_object):
    voidlen=sizeof(c_void_p)
    length=len(bytearray_object)
    if padding:=(voidlen-length)%voidlen:
        length+=padding
        bytearray_object.extend(bytes(padding))
    return (c_void_p*(length//voidlen)).from_buffer(bytearray_object)

__all__=[attr for attr in dir(ctypes)
         if not attr.startswith('_') and attr not in globals()]
globals().update((attr,getattr(ctypes,attr)) for attr in __all__)
__all__.extend(('bytearray2void','setproctitle'))

if sys.platform=='linux':

    libc=CDLL(None)
    _prctl=libc.prctl
    _prctl.argtypes=(c_int,c_char_p)
    _prctl.restype=c_int
    # defined in sys/prctl.h (linux/prctl.h in linux platform)
    PR_SET_NAME=15

    def setproctitle(title):
        if isinstance(title,str):
            title=title.encode('utf8')
        if _prctl(PR_SET_NAME,title[:16])<0:
            raise Exception(f'setproctitle failed: {os.strerror(get_errno())}')
        return

elif 'bsd' in sys.platform:

    # need test
    # See document in https://man.openbsd.org/setproctitle.3 (especially the CAVEATS)
    libc=CDLL(None)
    _setproctitle=libc.setproctitle
    _setproctitle.argtypes=(c_char_p,c_char_p)
    _setproctitle.restype=None
    def setproctitle(title):
        if isinstance(title,str):
            title=title.encode('utf8')
        libc.setproctitle(b'%s',title)
        return

else:

    # not supported
    def setproctitle(title):
        return


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
