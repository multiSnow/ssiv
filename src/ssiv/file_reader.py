#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from _thread import allocate_lock
from functools import partial
from sys import exit as sysexit
from threading import Thread

from .constants import FRDR_RCNAME
from .file_reader_starter import FileReader_Load_Process
from .mc import Connection,MsgPipe
from .mp import Process
from .mt import start_thread
from .tools import *

__all__=['FileReader']

class FileReader:

    __slots__=(
        '_control_pipe','_lock','_closed','_watcher_lock','_config',
    )

    def __new__(cls,config,*args,**kwds):
        instance=super().__new__(cls)
        instance._control_pipe=None
        instance._lock=allocate_lock()
        instance._closed=False
        instance._watcher_lock=None
        instance._config=config
        return instance

    def _setup(self):
        control,remote=MsgPipe()
        kwds={}
        assert kwds.update(inhertance=getinheritance()) or 1
        proc=Process(name='file_reader',target=FileReader_Load_Process,
                     args=(remote,self._config),kwargs=kwds)
        proc.start()
        assert trace(__name__,f'process {proc.pid} start')
        self._control_pipe=control
        self._closed=False
        # join process in thread to avoid zombie
        self._watcher_lock=start_thread(target=self.watcher,args=(proc,))

    def _stat(self,touch=False):
        try:
            self._control_pipe.sends('ping' if touch else 'stat')
            resp,=self._control_pipe.recvs()
            return resp
        except AttributeError as e:
            # control_pipe is None
            self._join()
            return
        except Exception as e:
            # broken pipe or race condition
            # (e.g. pipe closed after pipe accessed but just before send_bytes)
            assert warn(__name__,'stat failed',exc=e)
            self._join()
            self._cleanup()
            return

    def _ping(self):
        return self._stat(touch=True) is None

    def _cleanup(self):
        if self._control_pipe is not None:
            pipe=self._control_pipe
            self._control_pipe=None
            pipe.close()

    def _join(self):
        if self._watcher_lock is not None:
            lock=self._watcher_lock
            self._watcher_lock=None
            with lock:pass

    def start(self):
        with self._lock:
            if self._ping():
                self._setup()

    def close(self):
        with self._lock:
            self._closed=True
            if self._stat() is None:
                return
            self._control_pipe.sends('stop')
        self._join()

    def watcher(self,proc):
        proc.longjoin()
        assert trace(__name__,f'process {proc.pid} end {FRDR_RCNAME[proc.exitcode]}')
        proc.close()
        with self._lock:
            self._cleanup()

    def stat(self):
        with self._lock:
            if self._closed:
                return
            return self._stat()

    def read(self,pipe):
        with self._lock:
            if self._closed:
                pipe.send_bytes(b'')
                pipe.close()
                return
            if self._ping():
                self._setup()
            self._control_pipe.sends('read',pipe)

    def list(self,pipe):
        with self._lock:
            if self._closed:
                pipe.send_bytes(b'')
                pipe.close()
                return
            if self._ping():
                self._setup()
            self._control_pipe.sends('list',pipe)


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
