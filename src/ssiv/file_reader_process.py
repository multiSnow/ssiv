#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from _thread import allocate_lock
from functools import partial
from os.path import isdir,join
from time import monotonic
from zipfile import ZipFile,is_zipfile

from .co import AttrDict
from .constants import *
from .mt import *
from .scandir import *
from .tools import *

try:
    from chardet.universaldetector import UniversalDetector as Detector
except ImportError:
    Detector=None
else:
    from string import ascii_letters,digits
    printable=''.join((ascii_letters,digits,'.+-_()[]!$ '))

__all__=['FileReader_Process']

BLOCK_SIZE=4*1024*1024
HEAD_SIZE=64

class TaskPool(ThreadPool):

    __slots__=(
        '_timestamp',
    )

    def __init__(self):
        self.size=0
        self.touch()

    @property
    def timestamp(self):
        if not self.empty:
            self.touch()
        return self._timestamp

    def touch(self):
        self._timestamp=monotonic()

def openzip(path):
    zfile=ZipFile(path,mode='r')
    if Detector is None:
        assert info(__name__,f'open zipfile: {path}')
        return zfile
    names=set(''.join(s for s in info.filename.split('/')
                      if s.strip(printable)).encode('cp437')
              for info in zfile.infolist()
              if not (info.flag_bits&0x800 or info.is_dir()))
    if not names:
        assert info(__name__,f'open zipfile(utf8): {path}')
        return zfile
    zfile.close()
    detector=Detector()
    for name in sorted(names):
        detector.feed(name)
    assert info(__name__,'open zipfile({}):'.format(
        detector.close()['encoding'] or 'cp437'),path)
    return ZipFile(path,mode='r',metadata_encoding=detector.close()['encoding'])

def read_path(pipe,container,filename):
    fullpath=join(container,filename) if container else filename
    try:
        fp=open(fullpath,mode='rb')
        assert verb(__name__,f'open: {fullpath}')
    except Exception as e:
        assert warn(__name__,f'failed open: {fullpath}',exc=e)
        return True
    read=fp.read
    send_bytes=pipe.send_bytes
    recv_bytes=pipe.recv_bytes
    with fp:
        # send first block for ping image
        # is 64b enough?
        send_bytes(read(HEAD_SIZE))
        assert trace(__name__,f'read: {Pref.fixnum(HEAD_SIZE)}B {fullpath}')
        if recv_bytes():
            assert verb(__name__,f'interrupt reading: {fullpath}')
            # non-empty response, interrupt
            return False
        while data:=read(BLOCK_SIZE):
            send_bytes(data)
            assert trace(__name__,f'read: {Pref.fixnum(len(data))}B {fullpath}')
        assert trace(__name__,f'read: total {Pref.fixnum(fp.tell())}B {fullpath}')
    assert verb(__name__,f'close: {fullpath}')
    # send empty bytes as end.
    send_bytes(b'')
    return False

def read_zipfile(pipe,container,filename):
    send_bytes=pipe.send_bytes
    recv_bytes=pipe.recv_bytes
    try:
        with container.open(filename,mode='r') as fp:
            assert verb(__name__,f'open:',prettypath(container.filename,filename))
            read=fp.read
            send_bytes=pipe.send_bytes
            recv_bytes=pipe.recv_bytes
            send_bytes(read(HEAD_SIZE))
            assert trace(__name__,f'read: {Pref.fixnum(HEAD_SIZE)}B',
                         prettypath(container.filename,filename))
            if recv_bytes():
                assert verb(__name__,f'interrupt reading:',
                            prettypath(container.filename,filename))
                # non-empty response, interrupt
                return False
            while data:=read(BLOCK_SIZE):
                send_bytes(data)
                assert trace(__name__,f'read: {Pref.fixnum(len(data))}B',
                             prettypath(container.filename,filename))
            assert trace(__name__,f'read: total {Pref.fixnum(fp.tell())}B',
                         prettypath(container,filename))
    except Exception as e:
        assert warn(__name__,'failed open:',prettypath(container.filename,filename),exc=e)
        return True
    assert verb(__name__,'close:',prettypath(container.filename,filename))
    # send empty bytes as end.
    send_bytes(b'')
    return False

def list_path(pipe,container,recursive=False,follow=False):
    try:
        scanner=scandir(container,recursive=recursive,follow=follow)
        assert verb(__name__,f'scan: {container}')
    except Exception as e:
        assert warn(__name__,f'failed scan: {container}',exc=e)
        return True
    send_bytes=pipe.send_bytes
    with scanner:
        data=packstr(*scanner)
        datalen=len(data)
        assert trace(__name__,f'list: {Pref.fixnum(datalen)}B {container}')
        for pos in range(0,datalen,BLOCK_SIZE):
            send_bytes(data[pos:pos+BLOCK_SIZE])
    assert verb(__name__,f'leave: {container}')
    # send empty bytes as end.
    send_bytes(b'')
    return False

def list_zipfile(pipe,container,recursive=False,follow=False):
    # always list recursively in zip file
    # exclude directories and encrypted files
    send_bytes=pipe.send_bytes
    assert tuple(note(__name__,'skip encrypted file:',
                      prettypath(container.filename,info.filename))
                 for info in container.infolist() if info.flag_bits&0x1) or 1
    data=packstr(*(
        info.filename for info in container.infolist()
        if not info.is_dir() and not info.flag_bits&0x1
    ))
    datalen=len(data)
    assert trace(__name__,f'list: {Pref.fixnum(datalen)}B {container.filename}')
    for pos in range(0,datalen,BLOCK_SIZE):
        send_bytes(data[pos:pos+BLOCK_SIZE])
    # send empty bytes as end.
    send_bytes(b'')
    return False

def unsupported(*args,**kwds):
    return True

def detectpack(path,pool):
    # return AttrDict(container,lister,reader)
    if path not in pool:
        if isdir(path):
            assert info(__name__,f'detect as normal path: {path}')
            pool[path]=AttrDict(
                container=path,lister=list_path,reader=read_path)
        elif is_zipfile(path):
            try:
                pool[path]=AttrDict(
                    container=openzip(path),lister=list_zipfile,reader=read_zipfile)
            except Exception as e:
                assert warn(__name__,f'failed open: {path}',exc=e)
                pool[path]=AttrDict(
                    container=path,lister=unsupported,reader=unsupported)
        else:
            assert warn(__name__,f'unsupported container: {path}')
            pool[path]=AttrDict(
                container=path,lister=unsupported,reader=unsupported)
    return pool[path]

def command_read(pipe,lock,pool=None,**kwds):
    with pipe:
        if not (path:=pipe.recv_bytes()):
            assert verb(__name__,'cancel read')
            # non-empty response, interrupt
            return
        container,filename=unpackstr(path)
        assert trace(__name__,'command_read: start')
        with lock:
            x=detectpack(container,pool)
            if x.reader(pipe,x.container,filename,**kwds):
                pipe.send_bytes(b'')
                assert trace(__name__,'command_read: error')
    assert trace(__name__,'command_read: end')
    return

def command_list(pipe,lock,pool=None,**kwds):
    with pipe:
        if not (path:=pipe.recv_bytes()):
            assert verb(__name__,'cancel list')
            return
        container=path.decode('utf8')
        assert trace(__name__,'command_list: start')
        with lock:
            x=detectpack(container,pool)
            if x.lister(pipe,x.container,**kwds):
                pipe.send_bytes(b'')
                assert trace(__name__,'command_list: error')
        assert trace(__name__,'command_list: end')
    return

def FileReader_Process(control,config):
    taskpool=TaskPool()
    packpool={'':AttrDict(container='',lister=list_path,reader=read_path)}
    # serialize I/O
    lock=allocate_lock()
    # store as local variable
    poll=control.poll
    recvs=control.recvs
    sends=control.sends
    keepalive=config.keepalive
    readparam=dict(pool=packpool)
    scanparam=dict(recursive=config.recursive,follow=config.follow)
    assert trace(__name__,'loop in')
    while (timeout:=taskpool.timestamp+keepalive-monotonic())>0:
        assert trace(__name__,f'recv: wait {timeout:5.3f}')
        if not poll(timeout):
            continue
        data,*args=recvs()
        assert trace(__name__,f'recv: {data}')
        match data:
            case 'ping':
                sends('pong')
                taskpool.touch()
            case 'stat':
                sends('idle' if taskpool.empty else 'busy')
            case 'stop':
                break
            case 'read':
                pipe,=args
                taskpool.apply_async(command_read,(pipe,lock),readparam)
            case 'list':
                pipe,=args
                taskpool.apply_async(command_list,(pipe,lock),readparam|scanparam)
            case _:
                assert warn(__name__,f'unknown command: {data}')
    control.close()
    assert trace(__name__,'loop out')
    taskpool.join()
    for x in packpool.values():
        if isinstance(x.container,ZipFile):
            assert info(__name__,f'close zipfile: {x.container.filename}')
            x.container.close()
    assert trace(__name__,'end')
    return FRDR_SUCCESS


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
