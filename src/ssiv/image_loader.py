#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from _thread import allocate_lock
from collections import deque
from os import cpu_count

from .constants import WAND_RCNAME,WAND_PARTIAL_SEND
from .image_loader_starter import WandProcess
from .mc import Pipe
from .mp import Process
from .mt import start_thread
from .tools import *

class WandLoader:

    __slots__=(
        'procs','safelock','recver','sender','tasks','looplock',
        '_file_reader','_user_interface','_limit',
    )

    def __new__(cls,*args,**kwds):
        instance=super().__new__(cls)
        instance.procs=set()
        instance.safelock=allocate_lock()
        instance.recver,instance.sender=Pipe()
        instance.tasks=deque()
        instance.looplock=None
        instance._limit=0
        return instance

    def __init__(self,file_reader,user_interface,limit=None):
        self._file_reader=file_reader
        self._user_interface=user_interface
        self.limit=limit
        self.looplock=start_thread(target=self.mainloop)

    @property
    def file_reader(self):
        return self._file_reader

    @property
    def user_interface(self):
        return self._user_interface

    @property
    def limit(self):
        return self._limit

    @limit.setter
    def limit(self,value):
        match value:
            case 0:
                value=None
            case -1:
                value=0
        if value is None:
            value=cpu_count() or 1
        assert isinstance(value,int)
        assert value>-1
        self._limit=value

    def mainloop(self,interval=1):
        poll=self.recver.poll
        recv=self.recver.recv_bytes
        assert trace(__name__,'mainloop start')
        try:
            while True:
                if not poll(interval):
                    continue
                recv()
                with self.safelock:
                    assert (len(self.tasks)==1 and self.tasks[0] is None) or \
                        None not in self.tasks
                    if not self.tasks:continue
                    if self.limit and len(self.procs)>=self.limit:continue
                    if (task:=self.tasks.popleft()) is None:
                        if not self.procs:break
                        self.tasks.append(None)
                        continue
                    container,filename,kwds=task
                assert kwds.update(inhertance=getinheritance()) or 1
                if self.start_loader(container,filename,kwds):
                    self.feedback_cancel(container,filename,**kwds)
        except Exception as e:
            assert error(__name__,'mainloop error',exc=e)
        finally:
            assert trace(__name__,'mainloop end')

    def join(self):
        if self.looplock.acquire(timeout=5):
            self.looplock.release()
            return
        with self.safelock:
            if not self.procs:return
            for p in self.procs:
                p.kill()
        return self.join()

    def stop(self):
        return self.purge(stop=True)

    def purge(self,stop=False):
        with self.safelock:
            while self.tasks:
                container,filename,kwds=self.tasks.popleft()
                self.feedback_cancel(container,filename,**kwds)
            if stop:self.tasks.append(None)
            self.sender.send_bytes(b'')

    def load(self,container,filename,kwds,top=False):
        with self.safelock:
            if len(self.tasks)==1 and self.tasks[0] is None:return
            action=self.tasks.appendleft if top else self.tasks.append
            action((container,filename,kwds))
            self.sender.send_bytes(b'')

    def start_loader(self,container,filename,kwds):
        pipe_fr2il_fr,pipe_fr2il_il=Pipe()
        pipe_il2ui_il,pipe_il2ui_ui=Pipe()
        assert trace(__name__,'command_load: send to ui')
        if self.user_interface.speak('load',pipe_il2ui_ui):return
        assert trace(__name__,'command_load: file_reader.read')
        self.file_reader.read(pipe_fr2il_fr)
        assert trace(__name__,'command_load: start WandLoader')
        assert (p:=Pref.start(
            name=f'{__name__}: start loader {prettypath(container,filename)}'))
        args=(pipe_fr2il_il,pipe_il2ui_il,container,filename)
        proc=Process(name='wand_loader',target=WandProcess,
                     args=args,kwargs=kwds)
        assert p.record(name='init procss')
        try:
            proc.start()
            assert p.record(name='start procss')
        except Exception as e:
            assert p.record(name='start procss (failed)')
            assert error(__name__,f'process {proc.pid} failed',exc=e)
            pipe_fr2il_il.send_bytes(b'')
            pipe_il2ui_il.send_bytes(b'')
            assert p.log()
            return True
        assert trace(__name__,f'process {proc.pid} start')
        with self.safelock:
            assert proc not in self.procs
            self.procs.add(proc)
            self.sender.send_bytes(b'')
            start_thread(target=self.watch_loader,args=(proc,*args),kwds=kwds)
        assert p.record(name='watch procss')
        assert p.log()
        return

    def watch_loader(self,proc,pipe_fr,pipe_ui,container,filename,**kwds):
        proc.longjoin()
        rc=proc.exitcode
        assert trace(__name__,f'process {proc.pid} end {WAND_RCNAME[rc]}')
        with self.safelock:
            assert proc in self.procs
            self.procs.remove(proc)
            self.sender.send_bytes(b'')
            match WAND_RCNAME[rc]:
                case -9|-11:
                    pipe_fr.close()
                    pipe_ui.close()
                    self.feedback_cancel(container,filename,**kwds)
                case 'WAND_PARTIAL_SEND':
                    self.feedback_cancel(container,filename,**kwds)
                case 'WAND_UNSUPPORTED_FILE':
                    self.feedback_delete(container,filename)

    def feedback_cancel(self,container,filename,thumbnail=0,**kwds):
        self.user_interface.speak('cancel',container,filename,
                                  'thumbnail' if thumbnail else 'canvas')

    def feedback_delete(self,container,filename):
        self.user_interface.speak('delete',container,filename)


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
