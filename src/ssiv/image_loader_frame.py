#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from struct import pack,unpack,calcsize

from .constants import *

__all__=[
    'generate_crop_points',
    'FrameProperty','DataBuffer',
    'IMAGE_HEAD_FMT',
    'DISPOSE_NONE','DISPOSE_BACKGROUND','DISPOSE_PREVIOUS',
    'BLEND_NONE','BLEND_BLEND',
]

# loop, n_frames, is_thumbnail
IMAGE_HEAD_FMT='QQ?'

# attach FrameProperty to Texture but release DataBuffer once Texture updated

def generate_crop_points(width,height,blen):
    for x in range(0,width+(blen-width)%blen,blen):
        for y in range(0,height+(blen-height)%blen,blen):
            yield x,y,min(blen,width-x),min(blen,height-y)

class FrameProperty:

    _slot_names_=(
        ('delay','Q'),
        ('dispose','B'),
        ('blend','B'),
        ('bpp','B'),
        ('depth','B'),
        ('_pixfmt','B'),
        ('blen','Q'),
        ('width','Q'),
        ('height','Q'),
        ('page_width','Q'),
        ('page_height','Q'),
        ('offset_x','Q'),
        ('offset_y','Q'),
    )
    _pixfmts_=(None,'GRAY','GRAYA','INDEX','RGB','RGBA')
    _prop_head_fmt_='<{}'.format(''.join(v for _,v in _slot_names_))
    HEAD_SIZE=calcsize(_prop_head_fmt_)
    __slots__=(
        k for k,_ in _slot_names_
    )

    def __init__(self,delay=None,dispose=None,blend=None,
                 bpp=None,depth=None,pixfmt=None,blen=None,
                 width=None,height=None,page_width=None,page_height=None,
                 offset_x=None,offset_y=None):

        if isinstance(pixfmt,int):
            pixfmt=self._pixfmts_[pixfmt]
        self.delay=delay
        self.dispose=dispose
        self.blend=blend
        self.bpp=bpp
        self.depth=depth
        self.pixfmt=pixfmt
        self.blen=blen
        self.size=width,height
        self.page_size=page_width,page_height
        self.offset=offset_x,offset_y

    def __repr__(self):
        return f'{self.__class__.__name__}({{}})'.format(
            ','.join(f'{attr}={getattr(self,attr)}' for attr in (
                'delay','dispose','blend','bpp','depth','pixfmt','blen',
                'width','height','page_width','page_height','offset_x','offset_y',
            )))

    @classmethod
    def from_bytes(cls,data):
        return cls(*unpack(cls._prop_head_fmt_,data))

    def tobytes(self):
        return pack(self._prop_head_fmt_,
                    self.delay,self.dispose,self.blend,
                    self.bpp,self.depth,self._pixfmt,self.blen,
                    *self.size,*self.page_size,*self.offset)

    def copy(self):
        return FrameProperty.from_bytes(self.tobytes())

    @property
    def pixfmt(self):
        return self._pixfmts_[self._pixfmt]

    @pixfmt.setter
    def pixfmt(self,value):
        self._pixfmt=self._pixfmts_.index(value)

    @property
    def size(self):
        return self.width,self.height

    @size.setter
    def size(self,value):
        self.width,self.height=value

    @property
    def page_size(self):
        return self.page_width,self.page_height

    @page_size.setter
    def page_size(self,value):
        self.page_width,self.page_height=value

    @property
    def offset(self):
        return self.offset_x,self.offset_y

    @offset.setter
    def offset(self,value):
        self.offset_x,self.offset_y=value

class DataBuffer:

    __slots__=('pixels','palette')

    def __init__(self,pixels=None,palette=None):
        self.pixels=pixels
        self.palette=palette

    def __repr__(self):
        return f'{self.__class__.__name__}(pixels={{}},palette={{}})'.format(
            f'bytes({len(self.pixels)})' if self.pixels else None,
            f'bytes({len(self.palette)})' if self.palette else None,
        )


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
