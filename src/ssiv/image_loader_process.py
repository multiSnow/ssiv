#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from functools import partial
from struct import pack

from .constants import *
from .image_loader_frame import *
from .image_loader_generator import *
from .tools import *
from .wa import Image,getfmt

__all__=['Wand_Process']

BLOCK_SIZE=4*1024*1024
THUMBNAIL_SIZE=128 # hard limit of thumbnail size

def Wand_Process(reader_pipe,ui_pipe,container,filename,/,*,
                 maxlen=None,blen=None,options=None,
                 load_anime=False,load_apng=False,thumbnail=None,hint=None):
    # load raw data of image file in child process and send back to main process

    # reader_pipe: Connection to file_reader_process.
    # ui_pipe: Connection to ui_process.
    # maxlen: if not None, image will be shrinked to this length.
    # blen: crop static image to blocks of <blen>x<blen> if larger than such size.
    #       shrink animation to <blen>x<blen>, use smaller size of this and maxlen.
    # options: extra options pass to ImageMagick `-defines`.
    # load_anime: True to load image as animation if has multiple frame.
    #             False to load static image even if has multiple frame.
    # load_apng: True to load APNG as animation if has multiple frame.
    #            False to load APNG as regular PNG, no animation.
    # thumbnail: if not None, should be the length of thumbnail.
    # TODO:
    # hint: image format hint

    path=packstr(container,filename)
    is_apng=None
    shrink=None
    options={} if options is None else options.copy()
    generator=None
    n_loop=n_frames=None
    is_thum=False

    assert (p:=Pref.start(name=f'{__name__}: wandload process {prettypath(container,filename)}'))
    send_bytes=reader_pipe.send_bytes
    # do not use recv_bytes_into here for unknown length file support (maybe?)
    recv_bytes=reader_pipe.recv_bytes
    assert trace(__name__,f'request file: {prettypath(container,filename)}')
    send_bytes(path)
    # receive first block for ping image
    data=bytearray(recv_bytes())
    assert p.record(name='receive header blob')
    if not data:
        assert verb(__name__,'failed load')
        ui_pipe.send_bytes(b'')
        return WAND_UNSUPPORTED_FILE
    assert trace(__name__,f'recv blob: {Pref.fixnum(len(data))}B')

    try:
        imgfmt=hint or getfmt(data)
        if imgfmt is None:
            assert trace(__name__,'detect ping')
            with Image.ping(blob=bytes(data)) as im:
                imgfmt=im.format
            if imgfmt is None:
                raise ValueError('image format is invalid')
            assert info(__name__,f'detect ping: {imgfmt}',
                        prettypath(container,filename))
        else:
            assert info(__name__,f'detect magic: {imgfmt}',
                        prettypath(container,filename))
    except Exception as e:
        # unsupported data, interrupt
        send_bytes(b'unsupported file')
        ui_pipe.send_bytes(b'')
        assert warn(__name__,'unsupported file:',
                    prettypath(container,filename),
                    prettybytes(data[:16]),exc=e)
        return WAND_UNSUPPORTED_FILE
    assert p.record(name='first ping')
    send_bytes(b'')

    # file format recognized, continue to receive data
    while block:=recv_bytes():
        data.extend(block)
        assert trace(__name__,f'recv blob: {Pref.fixnum(len(block))}B',
                     prettypath(container,filename))
    assert p.record(name='full data received')

    if imgfmt in ('JPEG',):
        # shrink-on-load
        # As current implementation wand-py,
        # size should set before image object created,
        # so use Image.ping to get size before full decode.
        # Currently only JPEG support shrink-on-load
        #
        # FLIF should also support shrink-on-load,
        # by using flif_decoder_set_fit or flif_decoder_set_resize,
        # but ImageMagick uses neither of them.
        # And, ImageMagick does not use flif_read_info_from_memory in MagickPingImageFile,
        # caused full decoding full image in Image.ping method, unexpectedly
        assert trace(__name__,'ping image')
        try:
            with Image.ping(blob=data) as im:
                imgfmt=im.format
                size=(im.width,im.height)
                page=(*im.page,)
            if imgfmt is None:
                raise ValueError('image format is invalid')
        except Exception as e:
            # unsupported data, interrupt
            ui_pipe.send_bytes(b'')
            assert warn(__name__,'unsupported file:',
                        prettypath(container,filename),
                        prettybytes(data[:16]),exc=e)
            return WAND_UNSUPPORTED_FILE
        assert p.record(name=f'second ping {imgfmt}')
        if thumbnail:
            if max(size)>thumbnail and (maxlen or thumbnail+1)>thumbnail:
                # generate thumbnail, ignore maxlen and blen
                # currently only for JPEG
                if imgfmt=='JPEG':
                    tlen=min(thumbnail,THUMBNAIL_SIZE)
                    is_thum=True
                    assert verb(__name__,f'JPEG thumbnail: {tlen}x{tlen}')
                    options['jpeg:size']=f'{tlen}x{tlen}'
        if not is_thum:
            max_size=(maxlen,maxlen) if maxlen else None
            if (shrink:=get_shrink_size(size,page,max_size)) is not None:
                if imgfmt=='JPEG':
                    max_w,max_h=shrink
                    assert verb(__name__,f'JPEG shrink-on-load: {max_w}x{max_h}')
                    options.setdefault('jpeg:size',f'{max_w}x{max_h}')
    elif shrink is None:
        if thumbnail:
            is_thum=True
            shrink=(thumbnail,thumbnail)
        else:
            shrink=(maxlen,maxlen) if maxlen else None

    # disable anime if decode as thumbnail
    load_anime=load_anime and not is_thum
    # PNG format should be detected by getfmt, so it is safe to decide load_apng here
    load_apng=imgfmt=='PNG' and load_anime and load_apng and isAPNG(data)
    if load_apng:
        assert p.record(name='detect APNG')
        # crc already checked in APNG detection, ignore to speed up decoding
        options['png:ignore-crc']='true'
        n_loop,n_frames,chunkdict=open_APNG(data)
        assert p.record(name='disassemble APNG')
        data.clear()
        if blen:
            # blen not supported for APNG now, use shrink
            if shrink is None:
                shrink=(blen,blen)
            else:
                sw,sh=shrink
                shrink=(min(sw,blen),min(sh,blen))
        generator=generator_APNG(chunkdict,options=options,shrink=shrink,cls=Image)

    else:
        try:
            assert p.record(name='decode start')
            im=Image(blob=data,options=options)
            assert p.record(name='decoded')
            imgfmt=im.format
            if imgfmt is None:
                raise ValueError('image format is invalid')
        except Exception as e:
            if isinstance(im,Image):
                im.close()
            # unsupported data, interrupt
            ui_pipe.send_bytes(b'')
            assert warn(__name__,'unsupported file:',
                        prettypath(container,filename),
                        prettybytes(data[:16]),exc=e)
            return WAND_UNSUPPORTED_FILE
        data.clear()
        n_loop,n_frames=im.loop,im.iterator_length()
        load_anime=load_anime and n_frames>1
        if load_anime and blen:
            # animation not support blen for now, use shrink
            if shrink is None:
                shrink=(blen,blen)
            else:
                sw,sh=shrink
                shrink=(min(sw,blen),min(sh,blen))
        if not options:
            options=None
        if load_anime:
            generator=generator_Anime(im,options=options,shrink=shrink)
        else:
            n_frames=1
            generator=generator_Signle(im,options=options,shrink=shrink,blen=blen)

    # this assert should never happened
    assert None not in (n_loop,n_frames,generator) is not None,'generator undecided'

    try:
        send_bytes=ui_pipe._send_bytes # hack, avoid useless op in send_bytes
    except AttributeError:
        send_bytes=ui_pipe.send_bytes
    recv_bytes=partial(ui_pipe.recv_bytes,timeout=1)

    send_bytes(path)
    assert verb(__name__,'send init')
    send_bytes(pack(IMAGE_HEAD_FMT,n_loop,n_frames,is_thum))
    assert p.record(name='send init')
    n=0
    while True:
        n+=1
        try:
            frameprop,databuffer=next(generator)
        except StopIteration:
            break
        except Exception as e:
            assert warn(__name__,f'skip corrupt frame {n}/{n_frames}',exc=e)
            continue
        assert p.record(name=f'generate frame {n}/{n_frames}')
        # pause before send each frame
        if recv_bytes():
            assert verb(__name__,'interrupt')
            return WAND_PARTIAL_SEND
        send_bytes(frameprop.tobytes())

        # pause before send pixels
        if recv_bytes():
            assert verb(__name__,'interrupt')
            return WAND_PARTIAL_SEND
        pixels=databuffer.pixels
        databuffer.pixels=None
        pixels_length=len(pixels)
        send_bytes(pack('<Q',pixels_length))
        assert trace(__name__,f'send pixels: {Pref.fixnum(pixels_length)}B')
        for pos in range(0,pixels_length,BLOCK_SIZE):
            # pause before send each pixels block
            if recv_bytes():
                assert verb(__name__,'interrupt')
                return WAND_PARTIAL_SEND
            send_bytes(pixels[pos:pos+BLOCK_SIZE])
            assert trace(__name__,'send pixels:',
                         f'{min(pixels_length,pos+BLOCK_SIZE)/pixels_length:7.2%}')
        del pixels

        # pause before send palette
        if recv_bytes():
            assert verb(__name__,'interrupt')
            return WAND_PARTIAL_SEND
        palette=databuffer.palette
        databuffer.palette=None
        palette_length=len(palette)
        send_bytes(pack('<Q',palette_length))
        assert trace(__name__,f'send palette: {Pref.fixnum(palette_length)}B')
        for pos in range(0,palette_length,BLOCK_SIZE):
            # pause before send each palette block
            if recv_bytes():
                assert verb(__name__,'interrupt')
                return WAND_PARTIAL_SEND
            send_bytes(palette[pos:pos+BLOCK_SIZE])
            assert trace(__name__,'send palette:',
                         f'{min(palette_length,pos+BLOCK_SIZE)/palette_length:7.2%}')
        del palette

        assert trace(__name__,f'send frame {n}/{n_frames}')
        assert p.record(name=f'send frame {n}/{n_frames}')

    # pause before send false-bytes to finalize
    if recv_bytes():
        assert verb(__name__,'interrupt')
        rc=WAND_PARTIAL_SEND
    else:
        rc=WAND_SUCCESS
        # False-bytes as frame means no more frame to send
        send_bytes(b'')
    assert p.record(name=f'process end')
    assert verb(__name__,'all frames sent')
    assert p.log()
    return rc


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
