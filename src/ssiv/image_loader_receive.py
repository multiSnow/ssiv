#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from functools import partial
from struct import pack,unpack,calcsize,error as struct_error

from .image_loader_frame import *
from .tools import *

__all__=['WandReceive']

def _receive(pipe,env):
    send_bytes=pipe.send_bytes
    recv_bytes=pipe.recv_bytes
    recv_bytes_into=pipe.recv_bytes_into
    # receive n_loop and n_frames before first frame
    try:
        assert verb(__name__,'wait for init')
        n_loop,n_frames,is_thum=unpack(
            IMAGE_HEAD_FMT,recv_bytes(calcsize(IMAGE_HEAD_FMT)))
        yield n_loop,n_frames,is_thum
    except (EOFError,struct_error):
        assert warn(__name__,'init failed')
        return
    n=0
    while True:
        n+=1
        # require each frame
        if env.stop:
            assert verb(__name__,'interrupt')
            return send_bytes(b'stop')
        send_bytes(b'')
        if not (block:=recv_bytes(FrameProperty.HEAD_SIZE)):
            # received false-bytes, no more frame to receive
            assert verb(__name__,'all frames received')
            return
        frameprop=FrameProperty.from_bytes(block)
        databuffer=DataBuffer()

        # require pixels
        if env.stop:
            assert verb(__name__,'interrupt')
            return send_bytes(b'stop')
        send_bytes(b'')
        pixels_length,=unpack('<Q',recv_bytes(8))
        pixels_buffer=bytearray(pixels_length)
        position=0
        assert trace(__name__,f'recv pixels: {Pref.fixnum(pixels_length)}B')
        while position<pixels_length:
            # require each pixels block
            if env.stop:
                assert verb(__name__,'interrupt')
                return send_bytes(b'stop')
            send_bytes(b'')
            position+=recv_bytes_into(pixels_buffer,position)
            assert trace(__name__,f'recv pixels: {position/pixels_length:7.2%}')
        databuffer.pixels=pixels_buffer
        del pixels_buffer

        # require palette
        if env.stop:
            assert verb(__name__,'interrupt')
            return send_bytes(b'stop')
        send_bytes(b'')
        palette_length,=unpack('<Q',recv_bytes(8))
        palette_buffer=bytearray(palette_length)
        position=0
        assert trace(__name__,f'recv palette: {Pref.fixnum(palette_length)}B')
        while position<palette_length:
            # require each palette block
            if env.stop:
                assert verb(__name__,'interrupt')
                return send_bytes(b'stop')
            send_bytes(b'')
            position+=recv_bytes_into(palette_buffer,position)
            assert trace(__name__,f'recv pixels: {position/palette_length:7.2%}')
        databuffer.palette=palette_buffer
        del palette_buffer

        # yield frame
        assert trace(__name__,f'recv frame {n}/{n_frames}')
        yield frameprop,databuffer

def WandReceive(pipe,env):
    # generator of (n_loop, n_frames) and each frames, close pipe before end
    with pipe:
        try:
            yield from _receive(pipe,env)
        except Exception as e:
            pipe.send_bytes(b'stop')
            assert error(__name__,'recv error',exc=e)


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
