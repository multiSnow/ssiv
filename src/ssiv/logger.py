#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from multiprocessing import Pipe
from multiprocessing.connection import wait as waitpipe
from os import environ,getpid
from struct import calcsize,pack,unpack
from sys import stdout,stderr
from threading import current_thread,main_thread
from time import localtime,strftime,time_ns
from time import monotonic_ns,perf_counter_ns,process_time_ns
from traceback import format_exception

from .constants import progname
from .mt import start_thread

try:
    from psutil import Process
except ImportError:
    Process=None

__all__=['Pref','getinheritance','setinheritance','logend','is_mainthread']

# epoch, epoch_ns, pid, is_main_thread, error
LOGHDRFMT='QII??' # max pid up to 2**22 for 64bit system
LOGHDRFMTLEN=calcsize(LOGHDRFMT)

class Pref:

    __slots__=('name','points','proc')
    time_suffix=(('ns',10**3,3),('µs',10**3,3),('ms',10**3,3),('s',60,2))
    binary_prefix=('','Ki','Mi','Gi','Ti','Pi','Ei','Zi','Yi')
    printer=None
    disabled=True

    @staticmethod
    def fmtdelta(p1,p2):
        timedelta=', '.join(Pref.fixtime(e-s) for s,e in zip(p1[:3],p2[:3]))
        memdelta=Pref.fixnum(p2[3]-p1[3],align=True)
        return f'{timedelta}, {memdelta}B'

    @staticmethod
    def fixtime(n):
        buf=[]
        for s,d,l in Pref.time_suffix:
            n,r=divmod(n,d)
            buf.append(f'{{:>{l}}}{{}}'.format(r or '',s if r else ' '*len(s)))
        buf.append('{:>3}{}'.format(n or '','m' if n else ' '))
        return ' {}'.format(''.join(reversed(buf)))

    @staticmethod
    def fixnum(n,align=False):
        if not n or not (s:=(n.bit_length()-1)//10):
            return f'{n:>+11}' if align else f'{n}'
        m=-1 if n<0 else 1
        p=s*10
        n,r=divmod(abs(n),1<<p)
        try:
            s=Pref.binary_prefix[s]
            return f'{n*m:>+5}.{r*1000>>p:>03}{s}' \
                if align else \
                   f'{n*m}.{r*1000>>p:>03}{s}'
        except IndexError:
            return f'{n*m:>+5}.{r*1000>>p:>03}C{s}i' \
                if align else \
                   f'{n*m}.{r*1000>>p:>03}C{s}i'

    @classmethod
    def start(cls,*args,**kwds):
        if cls.disabled:return cls(*args,**kwds)
        instance=cls(*args,**kwds)
        instance.record(name=kwds['name'])
        return instance

    def __bool__(self):
        return True

    def __init__(self,name=''):
        if self.disabled:return
        self.name=name
        self.points=[]
        self.proc=None if Process is None else Process()

    def __repr__(self):
        if self.disabled:return ''
        if not self.points:return f'no records for {self.name}'
        delp1=False
        if len(self.points)==1:
            delp1=True
            self.record(name=self.points[0][4])
        buf=[f'{self.fmtdelta(*self.points[p-1:p+1])}, {self.points[p][4]}'
             for p in range(1,len(self.points))]
        if len(buf)>1:
            buf.extend((' total:',self.fmtdelta(self.points[0],self.points[-1])))
        if delp1:
            self.points.pop(1)
        buf.insert(0,f'preformance records: {self.name}')
        return '\n'.join(buf)

    def __del__(self):
        if self.disabled:return
        self.points.clear()

    def log(self):
        if self.disabled:return True
        return self.printer(__name__,self)

    def gettime(self):
        if self.disabled:return True
        return monotonic_ns(),perf_counter_ns(),process_time_ns()

    if Process is None:
        def getmem(self):return 0
    else:
        def getmem(self):
            if self.disabled:return 0
            return self.proc.memory_full_info().uss

    def record(self,name=None,clear=False):
        if self.disabled:return True
        if clear:self.points.clear()
        self.points.append((*self.gettime(),self.getmem(),name))
        return True

_colordict=dict(
    blod=1,
    black=30,

    red=31,
    orange=33,
    yellow=93,
    green=32,
    cyan=36,
    lightblue=94,
    blue=34,
    purple=35,

    lightred=91,
    lightgreen=92,
    lightcyan=96,
    lightgrey=37,
    darkgrey=90,
    pink=95,
)

def true(*args,**kwds):
    return True

def _color(text,name=None):
    if name is None:return text
    return f'\x1b[{_colordict.get(name,0)}m{text}\x1b[0m'

def _strfepoch(epoch,ns=0,/):
    return strftime(f'%FT%T.{ns:>09}%z',localtime(epoch))

def _writelog(epoch,ns,pid,ismt,level,levelcolor,target,usecolor,buf,blank):
    timestamp=_strfepoch(epoch,ns)
    level=f'{level:^5}'
    pidcolor=None
    if usecolor:
        level=_color(level,levelcolor)
        if pid!=_master_pid:
            pidcolor='lightcyan'
        if not ismt:
            pidcolor='yellow'
    pid=_color(f'{pid:>5}',pidcolor)
    header=f'{timestamp} {level} {_master_pid} {pid}'
    text='\n'.join(f'{blank if n else header} {line}'
                   for n,line in enumerate(buf))
    buf.clear()
    return print(text,file=target)

def _recvfrompipe(pipe,append,args):
    try:
        if (data:=pipe.recv_bytes())==b'stop':
            pipe.send_bytes(b'stop')
            return True
    except Exception as e:
        # print('pipe failed',file=stderr)
        # print(''.join(format_exception(e)),file=stderr)
        return True
    epoch,ns,pid,ismt,error=unpack(LOGHDRFMT,data[:LOGHDRFMTLEN])
    logfnerr,logfnerr_usecolor,logfnout,logfnout_usecolor=args
    if not error and logfnout is None:return
    if error and logfnerr is None:return
    level,levelcolor,*contents=data[LOGHDRFMTLEN:].decode('utf8').splitlines()
    target,usecolor=(logfnerr,logfnerr_usecolor) \
        if error else (logfnout,logfnout_usecolor)
    append((epoch,ns,pid,ismt,level,levelcolor,target,usecolor,contents))
    return

def _logloop(pipes,config,*,interval=1):
    blank=' '*len(f'{_strfepoch(2**32-1)} {0:^5} {_master_pid} {0:>5}')
    logfnout=environ.get(f'{progname}_LOG_STDOUT',config.stdout)
    logfnerr=environ.get(f'{progname}_LOG_STDERR',config.stderr)
    colorconf=environ.get(f'{progname}_LOG_COLOR',config.color)
    logfnout_usecolor=logfnerr_usecolor=colorconf!='never'
    close_out=close_err=False
    if not logfnout:
        logfnout=stdout if logfnout=='' else None
    elif isinstance(logfnout,str):
        logfnout_usecolor=False or colorconf=='always'
        try:
            logfnout=open(logfnout,mode='at',encoding='utf8')
            close_out=True
        except Exception as exc:
            print(f'failed to open {logfnout}',file=stderr)
            print(''.join(format_exception(exc)),file=stderr)
            logfnout=None
    if not logfnerr:
        logfnerr=stderr if logfnerr=='' else None
    elif isinstance(logfnerr,str):
        logfnerr_usecolor=False or colorconf=='always'
        try:
            logfnerr=open(logfnerr,mode='at',encoding='utf8')
            close_err=True
        except Exception as exc:
            print(f'failed to open {logfnerr}',file=stderr)
            print(''.join(format_exception(exc)),file=stderr)
            logfnerr=None

    args=logfnerr,logfnerr_usecolor,logfnout,logfnout_usecolor
    buf=[]
    append=buf.append
    sort=buf.sort
    clear=buf.clear
    while pipes:
        try:
            if not (conns:=waitpipe(pipes,timeout=interval)):continue
        except Exception as exc:
            # print(f'wait failed {exc!r}',file=stderr)
            # remove closed pipe
            closed_pipes=set(pipe for pipe in pipes if pipe.closed)
            pipes-=closed_pipes
            closed_pipes.clear()
            continue
        for pipe in conns:
            while pipe.poll():
                if _recvfrompipe(pipe,append,args):
                    pipes.remove(pipe)
                    break
        sort()
        for info in buf:
            _writelog(*info,blank)
        clear()

    if close_out:
        logfnout.close()
        logfnout=None
    if close_err:
        logfnerr.close()
        logfnerr=None
    return

def log(modulename,*args,exc=None,devel=False,level='log',levelcolor=None,error=False):
    if '_master_pid' not in globals():
        setinheritance()
    *_,modulename=modulename.partition('.')
    time,ns=divmod(time_ns(),1000000000)
    buf=' '.join(f'{s}' for s in (f'{modulename}:',*args)).splitlines()
    buf[0:0]=(level,levelcolor)
    if devel and exc is not None:
        buf.extend(''.join(format_exception(exc)).splitlines())
    content=('\n'.join(buf)).encode('utf8')
    head=pack(LOGHDRFMT,time,ns,_pid,is_mainthread(),error)
    _pipe.send_bytes(b''.join((head,content)))
    return True

def logend():
    with _pipe:
        _pipe.send_bytes(b'stop')
        _pipe.recv_bytes()
    if _locker is not None:
        for recver in _recvers:
            recver.close()
        with _locker:pass
    return True

def is_mainthread():
    return current_thread() is main_thread()

def getinheritance():
    if '_master_pid' not in globals():
        setinheritance()
    recver,sender=Pipe()
    globals()['_recvers'].add(recver)
    return globals()['_master_pid'],sender

def setinheritance(ppid=None,sender=None,/,config=None):
    lock=None
    recvers=None
    if ppid is None:
        ppid=getpid()
    globals()['_master_pid']=ppid
    if sender is None:
        recver,sender=Pipe()
        recvers={recver}
        lock=start_thread(target=_logloop,args=(recvers,config))
    globals().update(_pid=getpid(),_pipe=sender,
                     _recvers=recvers,_locker=lock)
    return True


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
