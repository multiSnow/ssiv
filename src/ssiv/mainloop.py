#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from _thread import allocate_lock
from functools import partial
from signal import SIG_IGN,SIGINT,SIGTERM,signal,strsignal

try:
    from signal import SIG_IGN,SIGINT,SIGTERM,SIGUSR1,SIGUSR2,signal,strsignal
except ImportError:
    SIGUSR1=SIGUSR2=None

from .co import AttrDict
from .config import *
from .constants import *
from .file_reader import FileReader
from .image_loader import WandLoader
from .mc import Pipe
from .mt import ThreadPool
from .tools import *
from .ui import UI

BLOCK_SIZE=4*1024*1024

def signal_close(sig,frame,ui=None):
    assert trace(__name__,f'signal hander: signal: {strsignal(sig)}')
    if sig in (SIGINT,SIGTERM):
        assert info(__name__,'signal hander: close ui')
        ui.close()
    assert trace(__name__,'signal hander: end')
    return

def command_list(path,ui,file_reader):
    assert path,'empty path is not allowed'
    assert verb(__name__,f'command_list: list: {path}')
    # 1, create pipe fr-ui
    pipe_fr2ui_fr,pipe_fr2ui_ui=Pipe()
    # 2, send to file_reader: fr-side of fr-ui and path
    assert trace(__name__,'command_list: file_reader.list')
    file_reader.list(pipe_fr2ui_fr)
    assert trace(__name__,'command_list: send to ui')
    # 3, send to ui: 'list', ui-side of fr-ui, path
    if ui.speak('list',pipe_fr2ui_ui,path):return
    assert trace(__name__,'command_list: end')
    return

def command_listed(filenames,ui):
    assert trace(__name__,'command_listed: start')
    path=''
    pipe_fr2ui_fr,pipe_fr2ui_ui=Pipe()
    if ui.speak('list',pipe_fr2ui_ui,path):return
    with pipe_fr2ui_fr:
        pipe_fr2ui_fr.recv_bytes()
        send_bytes=pipe_fr2ui_fr.send_bytes
        data=packstr(*filenames)
        datalen=len(data)
        assert trace(__name__,f'command_listed: {Pref.fixnum(datalen)}B')
        for pos in range(0,datalen,BLOCK_SIZE):
            send_bytes(data[pos:pos+BLOCK_SIZE])
        send_bytes(b'')
    assert trace(__name__,'command_listed: end')

def mainloop(*,images=(),containers=()):
    assert setinheritance(config=config.log)
    ui=UI()
    file_reader=FileReader(config.file_reader)
    wand_loader=WandLoader(file_reader,ui)
    wand_loader.limit=config.wand_loader.procs
    kwds_load=dict(
        blen=None,
        maxlen=config.wand_loader.maxsize,
        load_anime=config.wand_loader.anime,
        load_apng=config.wand_loader.apng,
    )
    kwds_thum=dict(
        load_anime=False,
        load_apng=False,
        thumbnail=config.thumbnail.maxsize,
    )
    ui.start(config)
    listener=ui.listen()
    handler=partial(signal_close,ui=ui)
    signal(SIGINT,handler)
    signal(SIGTERM,handler)
    if SIGUSR1 is not None:
        signal(SIGUSR1,handler)
        signal(SIGUSR2,handler)

    if images:
        command_listed(images,ui)
    for path in containers:
        command_list(path,ui,file_reader)

    assert trace(__name__,'loop in')
    while True:
        try:
            cmd,*args=next(listener)
        except StopIteration:
            assert verb(__name__,'ui stop')
            file_reader.close()
            wand_loader.stop()
            break
        if cmd is None:
            # make main thread not blocked too long
            continue
        assert trace(__name__,f'receive: {cmd}')
        match cmd:
            case 'purge':
                wand_loader.purge()
            case 'stat':
                pass # TODO: file_reader.stat()
            case 'load':
                container,filename,cutin=args
                wand_loader.load(container,filename,kwds_load,top=not not cutin)
            case 'thum':
                container,filename,cutin=args
                wand_loader.load(container,filename,kwds_thum,top=not not cutin)
            case 'max_texture_size':
                blen=min(args)
                if blen>0:
                    kwds_load['blen']=blen
                    assert note(__name__,f'max texture size: {blen}')
            case _:
                assert warn(__name__,f'unknown command: {cmd}')

    assert trace(__name__,'loop out')
    wand_loader.join()
    assert trace(__name__,'end')
    assert logend()


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
