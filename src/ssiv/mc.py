#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from _thread import allocate_lock
from io import BytesIO
from multiprocessing.connection import Pipe as _Pipe
#from multiprocessing.context import reduction
from struct import pack,unpack,calcsize

__all__=[
    'ConnectionRecvTimeout','Connection','Pipe','MsgPipe',
]

class ConnectionRecvTimeout(Exception):
    pass

#Pickler=reduction.ForkingPickler

class PipePickler:
    __slots__=('inbound','outbound','locker')
    def __new__(cls):
        instance=super().__new__(cls)
        instance.inbound,instance.outbound=_Pipe(False)
        instance.locker=allocate_lock()
        return instance
    def dumps(self,data):
        with self.locker:
            self.outbound.send(data)
            return self.inbound.recv_bytes()
    def loads(self,data):
        with self.locker:
            self.outbound.send_bytes(data)
            return self.inbound.recv()

class MsgConnection:

    __slots__=(
        'pickler',
    )

    def __new__(cls,*args,**kwds):
        instance=super().__new__(cls)
        instance.pickler=PipePickler()
        return instance

    def iterargs(self,args):
        for data in args:
            match data:
                case bytes():
                    yield 1,data
                case str():
                    yield 2,data.encode('utf8')
                case _:
                    yield 0,self.pickler.dumps(data)

    def iterdata(self,raw):
        with BytesIO(raw) as data:
            while head:=data.read(9):
                datatype,length=unpack('!BQ',head)
                match datatype:
                    case 1:
                        yield data.read(length)
                    case 2:
                        yield data.read(length).decode('utf8')
                    case _:
                        yield self.pickler.loads(data.read(length))

    def sends(self,*args):
        buf=[b''.join((pack('!BQ',datatype,len(data)),data))
             for datatype,data in self.iterargs(args)]
        length=sum(map(len,buf))
        buf.insert(0,pack('!BQ',1,length))
        data=b''.join(buf)
        buf.clear()
        self.send_bytes(data)

    def recvs(self):
        raw=self.recv_bytes()
        with BytesIO(raw) as data:
            datatype,length=unpack('!BQ',data.read(9))
            assert datatype==1
            result=(*self.iterdata(data.read(length)),)
            assert not data.read()
        return result

class Recver(MsgConnection):

    __slots__=(
        'recver','backer','locker','poll',
    )

    def __init__(self,recver,backer):
        self.__setstate__((recver,backer))

    def __getstate__(self):
        return self.recver,self.backer

    def __setstate__(self,state):
        self.recver,self.backer=state
        # initialize
        self.locker=allocate_lock()
        # directly inherted
        self.poll=self.recver.poll

    def recv_bytes(self,*args,**kwds):
        with self.locker:
            data=self.recver.recv_bytes(*args,**kwds)
            self.backer.send_bytes(pack('!B',1))
            return data

    def recv_bytes_into(self,*args,**kwds):
        with self.locker:
            length=self.recver.recv_bytes_into(*args,**kwds)
            self.backer.send_bytes(pack('!B',1))
            return length

    def recv(self,*args,**kwds):
        with self.locker:
            result=self.recver.recv(*args,**kwds)
            self.backer.send_bytes(pack('!B',1))
            return result

    def close(self):
        if hasattr(self,'recver'):
            self.recver.close()
            del self.poll,self.recver
        if hasattr(self,'backer'):
            self.backer.close()
            del self.backer

    def __del__(self):
        self.close()

class Sender(MsgConnection):

    __slots__=(
        'sender','backer','locker','inbuff',
    )

    def __init__(self,sender,backer):
        self.__setstate__((sender,backer))

    def __getstate__(self):
        assert not self.inbuff,f'non-zero inbuff: {self.inbuff}'
        return self.sender,self.backer

    def __setstate__(self,state):
        self.sender,self.backer=state
        # initialize
        self.locker=allocate_lock()
        self.inbuff=0

    def send_bytes(self,*args,**kwds):
        with self.locker:
            if self.inbuff:
                self.inbuff-=unpack('!B',self.backer.recv_bytes())[0]
            assert not self.inbuff,f'non-zero inbuff: {self.inbuff}'
            self.inbuff+=1
            self.sender.send_bytes(*args,**kwds)
            return

    def send(self,*args,**kwds):
        with self.locker:
            if self.inbuff:
                self.inbuff-=unpack('!B',self.backer.recv_bytes())[0]
            assert not self.inbuff,f'non-zero inbuff: {self.inbuff}'
            self.inbuff+=1
            self.sender.send(*args,**kwds)
            return

    def close(self):
        if hasattr(self,'sender'):
            self.sender.close()
            del self.sender
        if hasattr(self,'backer'):
            self.backer.close()
            del self.backer

    def __del__(self):
        self.close()

class Connection:

    __slots__=(
        'inbound','outbound','send','send_bytes','poll',
        '_recv','_recv_bytes','_recv_bytes_into',
    )
    tick=.01

    def __init__(self,inbound,outbound):
        self.__setstate__((inbound,outbound))

    def __getstate__(self):
        return self.inbound,self.outbound

    def __setstate__(self,state):
        self.inbound,self.outbound=state
        # directly inherted from outbound
        self.send=self.outbound.send
        self.send_bytes=self.outbound.send_bytes
        # directly inherted from inbound
        self.poll=self.inbound.poll
        # inserted from inbound
        self._recv=self.inbound.recv
        self._recv_bytes=self.inbound.recv_bytes
        self._recv_bytes_into=self.inbound.recv_bytes_into

    def __enter__(self):
        return self

    def __exit__(self,exc_type,exc_value,exc_tb):
        self.close()

    def _wait(self,timeout=None):
        while not self.poll(self.tick):
            if (timeout:=timeout-self.tick)<=0:
                raise ConnectionRecvTimeout()

    def recv(self,*args,timeout=None,**kwds):
        if timeout is not None:
            self._wait(timeout=timeout)
        return self._recv(*args,**kwds)

    def recv_bytes(self,*args,timeout=None,**kwds):
        if timeout is not None:
            self._wait(timeout=timeout)
        return self._recv_bytes(*args,**kwds)

    def recv_bytes_into(self,*args,timeout=None,**kwds):
        if timeout is not None:
            self._wait(timeout=timeout)
        return self._recv_bytes_into(*args,**kwds)

    def recvs(self):
        return self.inbound.recvs()

    def sends(self,*args):
        return self.outbound.sends(*args)

    def close(self):
        if hasattr(self,'inbound'):
            self.inbound.close()
            del self._recv,self._recv_bytes,self._recv_bytes_into,self.poll
            del self.inbound
        if hasattr(self,'outbound'):
            self.outbound.close()
            del self.send,self.send_bytes
            del self.outbound

    def __del__(self):
        self.close()

def OneWayPipe():
    iconn,oconn=_Pipe(False)
    ictrl,octrl=_Pipe(False)
    return Recver(iconn,octrl),Sender(oconn,ictrl)

def Pipe():
    i1,o1=_Pipe(False)
    i2,o2=_Pipe(False)
    return Connection(i1,o2),Connection(i2,o1)

def MsgPipe():
    i1,o1=OneWayPipe()
    i2,o2=OneWayPipe()
    return Connection(i1,o2),Connection(i2,o1)


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
