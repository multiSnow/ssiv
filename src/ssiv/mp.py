#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from _thread import allocate_lock
from multiprocessing import Lock,Process as _Process
from os import urandom
from signal import SIGINT,SIGTERM,SIG_DFL,SIG_IGN,signal
from sys import stderr
from traceback import print_exception

__all__=['Process']

class Process(_Process):

    # avoid race condition between start() and close()
    _lock=allocate_lock()

    def __init__(self,*args,timeout=3,**kwds):
        self.locker=Lock()
        self.hashid=urandom(256).__hash__()
        self.timeout=timeout
        kwds.setdefault('kwargs',{})['locker']=self.locker
        super().__init__(*args,**kwds)

    def __hash__(self):
        return self.hashid

    def __del__(self):
        self.close()

    def start(self):
        locker=self.locker
        self.locker=None
        locker.acquire()
        with self._lock:
            super().start()
            if locker.acquire(timeout=self.timeout):
                locker.release()
                return
            self.kill()
            self.longjoin()
            raise TimeoutError('Process.start timeout')

    def run(self):
        try:
            # lazy load in child process
            from .ct import setproctitle
            setproctitle(self.name)
            # ignore signal
            signal(SIGINT,SIG_IGN)
            signal(SIGTERM,SIG_IGN)
        except Exception as e:
            print(f'{self.name} setproctitle failed',file=stderr)
            print_exception(e,file=stderr)
        return super().run()

    def longjoin(self):
        self.join()
        while self.is_alive():
            self.join()

    def close(self):
        with self._lock:
            super().close()

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
