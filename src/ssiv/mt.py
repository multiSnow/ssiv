#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from _thread import allocate_lock,start_new_thread
from collections import deque
from os import cpu_count,urandom
from sys import stderr
from traceback import print_exception

__all__=['ThreadPool','start_thread']

def eprint(exc):
    return print_exception(exc,file=stderr)

def _starter(target,args,kwds,lock,startlock):
    assert lock.locked() and startlock.locked()
    startlock.release()
    target(*args,**kwds)
    lock.release()

def start_thread(*,target=None,args=(),kwds={}):
    assert callable(target)
    lock=allocate_lock()
    startlock=allocate_lock()
    lock.acquire()
    startlock.acquire()
    start_new_thread(_starter,(target,args,kwds,lock,startlock))
    with startlock:pass
    return lock

class Task:

    __slots__=(
        'target','args','kwds','onerror','oncancel',
    )

    def __new__(cls,target,args=(),kwds={},/,onerror=eprint,oncancel=None):
        instance=super().__new__(cls)
        instance.target=target
        instance.args=args
        instance.kwds=kwds
        instance.onerror=onerror
        instance.oncancel=oncancel
        return instance

    def __del__(self):
        if callable(self.oncancel):
            self.oncancel(*self.args,**self.kwds)

    def run(self):
        try:
            self.target(*self.args,**self.kwds)
        except Exception as e:
            if callable(self.onerror):
                self.onerror(e)
        self.oncancel=None

class ThreadPool:

    __slots__=(
        '_max_size','_runlock',
        '_threads','_threadlock',
        '_tasks','_tasklock',
    )

    def __new__(cls,size=None,/):
        instance=super().__new__(cls)
        instance._max_size=0
        instance._runlock=allocate_lock()
        instance._tasks=deque()
        instance._threads=set()
        # public method on _tasks should locked by _tasklock
        instance._tasklock=allocate_lock()
        # public method on _threads should locked by _threadlock
        instance._threadlock=allocate_lock()
        # None for auto detect, 0 for unlimited
        instance.size=size
        return instance

    @property
    def size(self):
        # pool size
        return self._max_size

    @size.setter
    def size(self,value):
        if value is None:
            value=cpu_count() or 1
        assert isinstance(value,int),f'size should be integer: {value}'
        assert value>-1,f'size should be zero or positive: {value}'
        self._max_size=value

    @property
    def empty(self):
        return not self._runlock.locked()

    def run_thread(self,threadid):
        # get and run next task
        while True:
            with self._tasklock:
                try:
                    task=self._tasks.popleft()
                except IndexError:
                    assert not self._tasks
                    with self._threadlock:
                        self._threads.remove(threadid)
                        if not self._threads:
                            self._runlock.release()
                    return
            task.run()

    def apply_async(self,target,args=(),kwds={},/,top=False,oncancel=None):
        # add 'target(*args,**kwds)' as latest task
        # if 'top' is True, add as first target
        # if task canceled and oncancel is callable, excute '(*args,**kwds)'
        with self._tasklock:
            action=self._tasks.appendleft if top else self._tasks.append
            action(Task(target,args,kwds,oncancel=oncancel))
            with self._threadlock:
                if self.size and len(self._threads)>=self.size:
                    return
                if not self._threads:
                    self._runlock.acquire()
                while (threadid:=urandom(256).__hash__()) in self._threads:pass
                self._threads.add(threadid)
                start_new_thread(self.run_thread,(threadid,))
            assert (self._threads and self._runlock.locked()) or \
                (not self._threads and not self._runlock.locked())

    def purge(self):
        # remove all tasks
        with self._tasklock:
            self._tasks.clear()

    def join(self):
        # wait until all existing tasks done
        with self._runlock:
            pass


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
