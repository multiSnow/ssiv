#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from os import scandir as _scandir
from posixpath import join

__all__=['scandir']

class scandir:

    __slots__=(
        '_recursive','_follow','_scanners',
    )

    def __init__(self,rootpath,recursive=False,follow=False):
        self._recursive=recursive # recursive scan
        self._follow=follow # whether follow symbolic link
        # [(parent, scanner), ...]
        # parent is None for root, or relative pathname from root
        # no need to use deque, this list is accessed LIFO
        self._scanners=[(None,_scandir(rootpath))]

    def __enter__(self):
        return self

    def __exit__(self,exc_type,exc_value,exc_tb):
        while self._scanners:
            parent,scanner=self._scanners.pop()
            scanner.close()

    def __next__(self):
        try:
            parent,scanner=self.current_scanner
            path=next(scanner)
        except IndexError:
            raise StopIteration
        except StopIteration:
            del self.current_scanner
            if parent is None:
                raise StopIteration
            return next(self)
        else:
            pathname=path.name if parent is None else join(parent,path.name)
            if path.is_file(follow_symlinks=self._follow):
                return pathname
            if self._recursive and path.is_dir(follow_symlinks=self._follow):
                try:
                    self.current_scanner=pathname,path
                except Exception as e:
                    # ignore inaccessible directory
                    pass
        return next(self)

    def __iter__(self):
        while True:
            try:
                yield next(self)
            except StopIteration:
                return

    @property
    def current_scanner(self):
        return self._scanners[-1]

    @current_scanner.setter
    def current_scanner(self,value):
        parent,path=value
        self._scanners.append((parent,_scandir(path)))

    @current_scanner.deleter
    def current_scanner(self):
        parent,scanner=self._scanners.pop()
        scanner.close()


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
