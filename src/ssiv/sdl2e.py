#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

if '__all__' not in globals():

    from ctypes import Array,string_at
    from struct import pack,unpack,calcsize
    import sdl2

    SIZEFMT='<Q'
    SIZEFMT_LEN=calcsize(SIZEFMT)

    class SDLE_PaletteColors(Array):
        _length_=256
        _type_=sdl2.SDL_Color
        def __init__(self,palette):
            self.raw=palette
            self.rawlen=rawlen=len(palette)
            d,m=divmod(rawlen,4)
            assert d<=self._length_,'palette too long'
            assert not m,'palette not in RGBA format'
            super().__init__(*(self._type_(*map(int,palette[n:n+4]))
                               for n in range(0,rawlen,4)))

        @property
        def length(self):
            return self.rawlen//4

    _all=set()

    # pairs of (sdl2 event type, (sdl2 event code, ...))
    evs=(
        ('SDL_QUIT',()),
        ('SDL_APP_TERMINATING',()),
        ('SDL_APP_LOWMEMORY',()),
        ('SDL_APP_WILLENTERBACKGROUND',()),
        ('SDL_APP_DIDENTERBACKGROUND',()),
        ('SDL_APP_WILLENTERFOREGROUND',()),
        ('SDL_APP_DIDENTERFOREGROUND',()),
        ('SDL_LOCALECHANGED',()),
        ('SDL_DISPLAYEVENT',(
            'SDL_DISPLAYEVENT_NONE',
            'SDL_DISPLAYEVENT_ORIENTATION',
            'SDL_DISPLAYEVENT_CONNECTED',
            'SDL_DISPLAYEVENT_DISCONNECTED',
        )),
        ('SDL_WINDOWEVENT',(
            'SDL_WINDOWEVENT_SHOWN',
            'SDL_WINDOWEVENT_HIDDEN',
            'SDL_WINDOWEVENT_EXPOSED',
            'SDL_WINDOWEVENT_MOVED',
            'SDL_WINDOWEVENT_RESIZED',
            'SDL_WINDOWEVENT_SIZE_CHANGED',
            'SDL_WINDOWEVENT_MINIMIZED',
            'SDL_WINDOWEVENT_MAXIMIZED',
            'SDL_WINDOWEVENT_RESTORED',
            'SDL_WINDOWEVENT_ENTER',
            'SDL_WINDOWEVENT_LEAVE',
            'SDL_WINDOWEVENT_FOCUS_GAINED',
            'SDL_WINDOWEVENT_FOCUS_LOST',
            'SDL_WINDOWEVENT_CLOSE',
            'SDL_WINDOWEVENT_TAKE_FOCUS',
            'SDL_WINDOWEVENT_HIT_TEST',
            'SDL_WINDOWEVENT_ICCPROF_CHANGED',
            'SDL_WINDOWEVENT_DISPLAY_CHANGED',
        )),
        ('SDL_SYSWMEVENT',()),
        ('SDL_KEYDOWN',()),
        ('SDL_KEYUP',()),
        ('SDL_TEXTEDITING',()),
        ('SDL_TEXTINPUT',()),
        ('SDL_KEYMAPCHANGED',()),
        ('SDL_TEXTEDITING_EXT',()),
        ('SDL_MOUSEMOTION',()),
        ('SDL_MOUSEBUTTONDOWN',()),
        ('SDL_MOUSEBUTTONUP',()),
        ('SDL_MOUSEWHEEL',()),
        ('SDL_JOYAXISMOTION',()),
        ('SDL_JOYBALLMOTION',()),
        ('SDL_JOYHATMOTION',()),
        ('SDL_JOYBUTTONDOWN',()),
        ('SDL_JOYBUTTONUP',()),
        ('SDL_JOYDEVICEADDED',()),
        ('SDL_JOYDEVICEREMOVED',()),
        ('SDL_JOYBATTERYUPDATED',()),
        ('SDL_CONTROLLERAXISMOTION',()),
        ('SDL_CONTROLLERBUTTONDOWN',()),
        ('SDL_CONTROLLERBUTTONUP',()),
        ('SDL_CONTROLLERDEVICEADDED',()),
        ('SDL_CONTROLLERDEVICEREMOVED',()),
        ('SDL_CONTROLLERDEVICEREMAPPED',()),
        ('SDL_CONTROLLERTOUCHPADDOWN',()),
        ('SDL_CONTROLLERTOUCHPADMOTION',()),
        ('SDL_CONTROLLERTOUCHPADUP',()),
        ('SDL_CONTROLLERSENSORUPDATE',()),
        ('SDL_FINGERDOWN',()),
        ('SDL_FINGERUP',()),
        ('SDL_FINGERMOTION',()),
        ('SDL_DOLLARGESTURE',()),
        ('SDL_DOLLARRECORD',()),
        ('SDL_MULTIGESTURE',()),
        ('SDL_CLIPBOARDUPDATE',()),
        ('SDL_DROPFILE',()),
        ('SDL_DROPTEXT',()),
        ('SDL_DROPBEGIN',()),
        ('SDL_DROPCOMPLETE',()),
        ('SDL_AUDIODEVICEADDED',()),
        ('SDL_AUDIODEVICEREMOVED',()),
        ('SDL_SENSORUPDATE',()),
        ('SDL_RENDER_TARGETS_RESET',()),
        ('SDL_RENDER_DEVICE_RESET',()),
        ('SDL_POLLSENTINEL',()),
    )

    # pairs of (user event type, (user event code, ...))
    userevs=(
        ('SDL_WAND',(
            'SDL_WAND_LOADINGSTARTED',
            'SDL_WAND_THUMBNAILSTARTED',
            'SDL_WAND_FRAMEDECODED',
            'SDL_WAND_THUMBNAILDECODED',
            'SDL_WAND_LOADINGFINISHED',
            'SDL_WAND_THUMBNAILFINISHED',
            'SDL_WAND_LOADCANCEL',
        )),
        ('SDL_SDLUI',(
            'SDL_SDLUI_VIEWCANVAS',
            'SDL_SDLUI_VIEWTILING',
            'SDL_SDLUI_CANVASDRAW',
            'SDL_SDLUI_ANIMECONTINUE',
            'SDL_SDLUI_MOTIONCONTINUE',
        )),
    )

    # {event type value:(event type name,{event code value:event code name})
    evmap={}

    globalmap=globals()

    # get the start number of user event types
    if (start:=sdl2.SDL_RegisterEvents(len(userevs)))==2**32-1:
        raise ImportError('SDL_RegisterEvents failed: no enough user-defined events left')

    # add all sdl2 modules and constants to global and expose names
    _all={attr for attr in dir(sdl2)
          if attr.startswith(('SDL_','SDLK_','KMOD_'))}
    globalmap.update((attr,getattr(sdl2,attr)) for attr in _all)

    for pixfmt in ('SDL_PIXELFORMAT_RGB24','SDL_PIXELFORMAT_RGBA32',
                   'SDL_PIXELFORMAT_INDEX1LSB','SDL_PIXELFORMAT_INDEX8'):
        if globalmap[pixfmt] not in sdl2.ALL_PIXELFORMATS:
            raise ImportError(f'pixel format not available: {pixfmt}')

    for ev,subevs in evs:
        # add sdl2 event types and codes to evmap
        evmap[globalmap[ev]]=(ev,{globalmap[subev]:subev for subev in subevs})

    for n,(ev,subevs) in enumerate(userevs,start):
        # add user event types to globals
        globalmap[ev]=n
        # add user event codes to globals
        globalmap.update((subev,c) for c,subev in enumerate(subevs))
        # add user event types and codes to evmap
        evmap[n]=(ev,{globalmap[subev]:subev for subev in subevs})
        # expose names of user event types and codes
        _all.add(ev)
        _all.update(subevs)

    class _EvEmu:
        _evtypes=tuple(name for name,_ in evmap.values())
        _evcodes=tuple(name for _,ev in evmap.values() for name in ev.values())
        __slots__=(*_evtypes,*_evcodes)
        def __init__(self):
            for name in self.__slots__:
                setattr(self,name,globalmap[name])

    SDLE_EvEmu=_EvEmu()
    SDLE_SDLEventTypes=(*(globalmap[ev] for ev,_ in evs),)
    SDLE_UserEventTypes=(*(globalmap[ev] for ev,_ in userevs),)

    SDLE_Scancodes={globalmap[c]:c for c in _all if c.startswith('SDL_SCANCODE_')}
    SDLE_Keycodes={globalmap[c]:c for c in _all if c.startswith('SDLK_')}
    SDLE_Keymods=(*sorted(((globalmap[c],c) for c in _all
                           if c.startswith('KMOD_')),reverse=True),)
    SDLE_Buttons={globalmap[c]:c for c in _all
                  if c.startswith('SDL_BUTTON_') and not c.endswith('MASK')}

    def SDLE_GetEventTypeName(etype):
        return evmap.get(etype,(f'unknown:{etype}',None))[0]

    def SDLE_GetEventCodeName(etype,eid):
        if etype not in evmap:
            return f'unknown event:{etype}'
        evname,subevmap=evmap[etype]
        return subevmap.get(eid,f'unknown eventid:{evname}:{etype}')

    def SDLE_GetEventCode(event):
        match SDLE_GetEventTypeName(etype:=event.type):
            case 'SDL_WINDOWEVENT':
                return etype,event.window.event
            case 'SDL_DISPLAYEVENT':
                return etype,event.display.event
            case 'SDL_WAND'|'SDL_SDLUI':
                return etype,event.user.code
            case _:
                return etype,None

    def SDLE_GetEventName(event):
        return SDLE_GetEventTypeName(event.type)

    def SDLE_GetEventFullName(event):
        etype,eid=SDLE_GetEventCode(event)
        s=[f'{SDLE_GetEventTypeName(etype)}']
        if eid is not None:
            s.append(f'{SDLE_GetEventCodeName(etype,eid)}')
        return ' '.join(s)

    pixfmt_alias_map={globalmap[pixfmt]:pixfmt for pixfmt in (
        'SDL_PIXELFORMAT_RGBA32',
        'SDL_PIXELFORMAT_ARGB32',
        'SDL_PIXELFORMAT_BGRA32',
        'SDL_PIXELFORMAT_ABGR32',
        # the name returned by SDL_GetPixelFormatName (e.g. RGB888) is misleading
        'SDL_PIXELFORMAT_RGBX8888',
        'SDL_PIXELFORMAT_XRGB8888',
        'SDL_PIXELFORMAT_BGRX8888',
        'SDL_PIXELFORMAT_XBGR8888',
    )}

    def SDLE_GetPixFmtName(pixfmt):
        try:
            return pixfmt_alias_map[pixfmt]
        except KeyError:
            return SDL_GetPixelFormatName(pixfmt).decode('utf8')

    def _getdata(ptr):
        if isinstance(ptr,int):
            size,=unpack(SIZEFMT,string_at(ptr,size=SIZEFMT_LEN))
            return string_at(ptr+SIZEFMT_LEN,size=size)
        return b''

    def _setdata(data):
        if not data:return
        assert isinstance(data,bytes),f'unsupported type: {type(data)}'
        size=len(data)
        ptr=SDL_malloc(size+SIZEFMT_LEN)
        SDL_memcpy(ptr,pack(SIZEFMT,size),SIZEFMT_LEN)
        SDL_memcpy(ptr+SIZEFMT_LEN,data,size)
        return ptr

    def _deldata(ptr):
        if isinstance(ptr,int):
            SDL_free(ptr)
        return

    def SDLE_GetUserEventData(event):
        assert event.type in SDLE_UserEventTypes,\
            f'unsupported event: {SDLE_GetEventName(event)}'
        return _getdata(event.user.data1),_getdata(event.user.data2)

    def SDLE_SetUserEventData(event,data,data2=None,/):
        assert event.type in SDLE_UserEventTypes,\
            f'unsupported event: {SDLE_GetEventName(event)}'
        p1=_setdata(data)
        p2=_setdata(data2)
        SDLE_DelUserEventData(event)
        event.user.data1=p1
        event.user.data2=p2

    def SDLE_DelUserEventData(event):
        assert event.type in SDLE_UserEventTypes,\
            f'unsupported event: {SDLE_GetEventName(event)}'
        event.user.data1=_deldata(event.user.data1)
        event.user.data2=_deldata(event.user.data2)

    def SDLE_GetScancodeName(scancode):
        try:
            return SDLE_Scancodes[scancode]
        except KeyError:
            return f'unknown scancode: {scancode}'

    def SDLE_GetKeycodeName(keycode):
        try:
            return SDLE_Keycodes[keycode]
        except KeyError:
            return f'unknown keycode: {keycode}'

    def SDLE_GetKeymodName(keymod,combine=False):
        names=set()
        for n,s in SDLE_Keymods:
            if not combine and n.bit_count()>1:continue
            if m:=(keymod&n):
                names.add(s)
                keymod^=m
        if keymod:
            names.add(f'KMOD_UNKNOWN_{keymod}')
        return names

    def SDLE_GetButtonName(button):
        try:
            return SDLE_Buttons[button]
        except KeyError:
            return f'SDL_BUTTON_EX_{button}'

    _all.update(attr for attr in globalmap if attr.startswith('SDLE_'))

    __all__=list(_all)


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
