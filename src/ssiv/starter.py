#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

import sys

from .constants import *

__all__=['start']

def check_version(config,locker=None):
    from . import tools
    tools.setinheritance(config=config.log)
    rc=0

    # python version and platform
    tools.info(__name__,'Python {}.{}.{} {} {}'.format(
        *sys.version_info[:3],sys.platform,
        '32-bit' if sys.maxsize==2**31-1 else '64-bit'))
    if sys.platform not in supported_platform:
        tools.warn(__name__,f'{sys.platform} platform may not be supported.')

    # PySDL2 and SDL version
    try:
        import sdl2
        if sdl2.version_info<required_pysdl2ver:
            raise ImportError(
                'PySDL2 {}.{}.{} is lower than {}.{}.{}'.format(
                    *sdl2.version_info,*required_pysdl2ver))
        sdl2.SDL_GetVersion(sdlver:=sdl2.SDL_version())
        sdlver=(sdlver.major,sdlver.minor,sdlver.patch)
        if sdlver<required_sdl2ver:
            raise ImportError(
                'SDL {}.{}.{} is lower than {}.{}.{}'.format(
                    *sdlver,*required_sdl2ver))
        from . import sdl2e
    except ImportError as e:
        tools.fault(__name__,f'PySDL2 not available: {e}')
        rc=1
    else:
        tools.info(__name__,'PySDL2 {}.{}.{} SDL {}.{}.{}'.format(
            *sdl2.version_info,*sdlver))

    # Wand and ImageMagick version
    try:
        from wand.version import MAGICK_VERSION_INFO,VERSION_INFO
        if VERSION_INFO<required_wandver:
            raise ImportError(
                'Wand {}.{}.{} is lower than {}.{}.{}'.format(
                    *VERSION_INFO,*required_wandver))
        if MAGICK_VERSION_INFO<required_magickver:
            raise ImportError(
                'ImageMagick {}.{}.{} is lower than {}.{}.{}'.format(
                    *MAGICK_VERSION_INFO,*required_magickver))
        from . import wa
    except ImportError as e:
        tools.fault(__name__,f'Wand not available: {e}')
        rc=11
    else:
        tools.info(__name__,'Wand {}.{}.{} ImageMagick {}.{}.{}'.format(
            *VERSION_INFO,*MAGICK_VERSION_INFO))
    tools.logend()
    locker.release()
    sys.exit(rc)

def check_3rdpart_library(config):
    from .mp import Process
    proc=Process(name='check_version',target=check_version,args=(config,))
    proc.start()
    proc.longjoin()
    rc=proc.exitcode
    proc.close()
    return not rc

def get_parser():
    from argparse import ArgumentParser
    argp=ArgumentParser(
        prog=progname.lower(),description='Simple SDL Image Viewer',add_help=False,
        usage='\n{}'.format('\n'.join(f'      %(prog)s {s}' for s in (
            '[-h] [-v]','[-d CONFIG]',
            '[-f CONFIG] [IMAGE ...] -c [CONTAINER ...]',
        ))),
        epilog='Support regular directory and nonencrypted zip archive as container.',
    )
    argp.add_argument('-h','--help',action='help',
                      help='show help message and exit')
    argp.add_argument('-v','--version',action='version',
                      version='%(prog)s {}.{}.{}'.format(*version_info),
                      help='show version and exit')
    argp.add_argument('images',nargs='*',default=[],
                      metavar='IMAGE',help='image file(s) to show as in one container')
    argp.add_argument('-f','--conf',action='store',dest='conf',
                      metavar='CONFIG',help='load custom config at CONFIG, - for STDIN')
    argp.add_argument('-d','--dump',action='store',dest='dump',
                      metavar='CONFIG',help='dump default config to CONFIG, - for STDOUT')
    argp.add_argument('-c','--container',dest='containers',nargs='*',default=[],
                      metavar='CONTAINER',help='container(s) to open')
    return argp

def normpaths(paths):
    from os.path import normpath
    results=[]
    append=results.append
    for path in map(normpath,paths):
        if path not in results:
            append(path)
    return (*results,)

def start():
    pyver=sys.version_info[:3]
    if pyver<required_pyver:
        print('Python {}.{}.{} is lower than '
              'minimal requirement {}.{}.{}'.format(
                  *pyver,*required_pyver),
              file=sys.stderr)
        return 1

    from .config import config,default,loadcustom,validate
    assert validate(config)

    parser=get_parser()
    args=parser.parse_args()
    if args.dump is not None:
        match args.dump:
            case '-':
                print(default)
            case _:
                try:
                    with open(args.dump,mode='wt',encoding='utf8') as fp:
                        print(default,file=fp)
                except Exception as e:
                    print(f'failed dump config to {args.dump}: {e}')
                    return 1
        return
    if not args.images and not args.containers:
        parser.print_usage(file=sys.stderr)
        print('At least one IMAGE or CONTAINER required.',file=sys.stderr)
        return 1
    if args.conf is not None:
        try:
            match args.conf:
                case '-':
                    loadcustom(sys.stdin.buffer)
                case _:
                    with open(args.conf,mode='rb') as fp:
                        loadcustom(fp)
            assert validate(config)
        except Exception as e:
            print(f'failed to load config: {e}')
            return 1

    from os import environ
    try:
        int(level:=environ.setdefault(f'{progname}_LOG_LEVEL',f'{config.log.level}'))
    except ValueError:
        print(f'invalid {progname}_LOG_LEVEL value: {level!r}')
        return 1
    if config.log.devel:
        environ.setdefault(f'{progname}_LOG_DEVEL','x')
    environ.update(
        (f'{option.upper()}_{key}',value)
        for option in config.environ
        for key,value in config.environ[option]
    )
    if config.wand_loader.procs!=1:
        # disable OpenMP of ImageMagick in concurrently decoding process
        environ.setdefault('MAGICK_THREAD_LIMIT','1')
    if config.renderer.prime:
        environ.setdefault('DRI_PRIME','1')

    try:
        assert check_3rdpart_library(config)
    except:
        return 1

    from .mainloop import mainloop
    return mainloop(images=normpaths(args.images),
                    containers=normpaths(args.containers))


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
