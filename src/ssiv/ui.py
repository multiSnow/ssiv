#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from _thread import allocate_lock
from functools import partial

from .constants import UI_RCNAME
from .mc import MsgPipe
from .mp import Process
from .mt import start_thread
from .tools import *
from .ui_starter import SDLUI_Load_Process

__all__=['UI']

UI_Load_Process=SDLUI_Load_Process

class UI:

    __slots__=(
        '_sender','_receiver','_lock','_closed','_watcher_lock',
    )

    def __new__(cls):
        instance=super().__new__(cls)
        instance._sender=None
        instance._receiver=None
        instance._lock=allocate_lock()
        instance._closed=True
        instance._watcher_lock=None
        return instance

    def __repr__(self):
        return self.__class__.__name__

    def _setup(self,config):
        control_sender,remote_receiver=MsgPipe()
        remote_sender,control_receiver=MsgPipe()
        kwds={}
        assert kwds.update(inhertance=getinheritance()) or 1
        proc=Process(name='user_interface',target=UI_Load_Process,
                     args=(remote_sender,remote_receiver,config),kwargs=kwds)
        proc.start()
        assert trace(__name__,f'process {proc.pid} start')
        self._sender=control_sender
        self._receiver=control_receiver
        self._closed=False
        # join process in thread to avoid zombie
        self._watcher_lock=start_thread(target=self.watcher,args=(proc,))

    def _cleanup(self,exitcode=None):
        if self._sender is not None:
            sender=self._sender
            self._sender=None
            sender.close()
        if self._receiver is not None:
            receiver=self._receiver
            self._receiver=None
            receiver.close()

    def start(self,config):
        with self._lock:
            if self._closed:
                self._setup(config)

    def close(self):
        with self._lock:
            if not self._closed:
                self._sender.sends('stop')

    def watcher(self,proc):
        proc.longjoin()
        assert trace(__name__,f'process {proc.pid} end {UI_RCNAME[proc.exitcode]}')
        proc.close()
        with self._lock:
            self._closed=True
            self._cleanup()

    def listen(self,interval=1):
        assert is_mainthread()
        poll=self._receiver.poll
        recvs=self._receiver.recvs
        assert trace(__name__,f'{self}: recv: loop in')
        while True:
            with self._lock:
                if self._closed:
                    break
            if not poll(interval):
                # make main thread not blocked too long
                yield None,
                continue
            try:
                cmd,*args=recvs()
            except (EOFError,ValueError):
                break
            if cmd=='over':
                yield 'purge',
                try:
                    self._sender.sends()
                except:
                    pass
                break
            yield cmd,*args
        assert trace(__name__,f'{self}: recv: loop out')
        with self._watcher_lock:pass
        assert trace(__name__,f'{self}: recv: end')

    def speak(self,*args):
        assert trace(__name__,f'{self}: speak start')
        with self._lock:
            if self._closed:
                assert trace(__name__,f'{self}: speak closed')
                return True
            try:
                self._sender.sends(*args)
            except Exception as e:
                assert verb(__name__,f'{self}: speak socket closed')
                self._closed=True
                self._cleanup()
                return True
            assert trace(__name__,f'{self}: speak',
                         args[0] if args else 'empty')


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
