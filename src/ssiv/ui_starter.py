#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

# do not import any module in toplevel

def SDLUI_Load_Process(sender,receiver,config,locker=None,**kwds):
    import sys
    from . import tools
    assert tools.setinheritance(*kwds.pop('inhertance'))
    assert not kwds
    try:
        # lazy import in child process
        from .sdlui_process import SDLUI_Process
        assert tools.verb(__name__,'start')
        locker.release()
        rc=SDLUI_Process(sender,receiver,config)
        assert tools.verb(__name__,'end')
    except Exception as e:
        rc=1
        assert tools.error(__name__,'failed with exception',exc=e)
    assert tools.logend()
    sys.exit(rc)


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
