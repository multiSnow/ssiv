#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from wand.image import IMAGE_TYPES,BaseImage,Image as _Image
from wand.api import library
from wand.version import formats

from .ct import c_int,c_void_p,bytearray2void

library.MagickIdentifyImageType.argtypes = [c_void_p]
library.MagickIdentifyImageType.restype = c_int

class Image(_Image):
    supported_formats=tuple(formats())
    _fmtmap={
        # format of image type, ignore 'alpha'/'matte' in palette type
        'bilevel':'GRAY',
        'grayscale':'GRAY',
        'grayscalealpha':'GRAYA',
        'grayscalematte':'GRAYA',
        'palette':'INDEX',
        'truecolor':'RGB',
        'truecoloralpha':'RGBA',
        'truecolormatte':'RGBA',
        'colorseparation':'CMYK', # need verify
        'colorseparationalpha':'CMYKA', # need verify
        'colorseparationmatte':'CMYKA', # need verify
        'palettebilevel':'INDEX',
    }
    _chlmap={
        # amounts of color in each pixel
        'INDEX':1,
        'GRAY':1,
        'GRAYA':2,
        'RGB':3,
        'RGBA':4,
        'CMYK':4, # need verify
        'CMYKA':5, # need verify
    }

    def __new__(cls,*args,**kwds):
        instance=super().__new__(cls)
        instance._identified_type=None
        return instance

    def __init__(self,*args,file=None,filename=None,blob=None,options=None,**kwds):
        blob_is_bytearray=isinstance(blob,bytearray)
        if not options and not blob_is_bytearray or \
           any((key in kwds) for key in ('image','pseudo')):
                # Image.read() does not support 'image' and 'pseudo' keywords
                return super().__init__(*args,file=file,filename=filename,blob=blob,**kwds)
        super().__init__(*args)
        self.options.update(options or {})
        if blob_is_bytearray:
            units=kwds.pop('units',None)
            self._preamble_read(**kwds)
            if library.MagickReadImageBlob(self.wand,bytearray2void(blob),len(blob)):
                if units is not None:self.units=units
            else:
                self.raise_exception()
        else:
            self.read(file=file,filename=filename,blob=blob,**kwds)

        if (identified_type:=self.identified_type)!=self.type:
            self.type=identified_type
        self.format=('RGBA' if self.alpha_channel else 'RGB') \
            if self.fmt=='INDEX' else self.fmt

    @classmethod
    def ping(cls,file=None,filename=None,blob=None,**kwds):
        if not isinstance(blob,bytearray):
            return super().ping(file=file,filename=filename,blob=blob,**kwds)
        instance=cls()
        instance._preamble_read(**kwds)
        if library.MagickPingImageBlob(instance.wand,bytearray2void(blob),len(blob)):
            units=kwds.get('units')
            if units is not None:
                instance.units=units
        return instance

    @property
    def animation(self):
        # fix for upstream
        # not only gif, but also many other formats support animation.
        return self.iterator_length()>1

    @property
    def identified_type(self):
        # identified image type
        if self._identified_type is None:
            if index:=library.MagickIdentifyImageType(self.wand):
                self._identified_type=IMAGE_TYPES[index]
            else:
                self.raise_exception()
        return self._identified_type

    @property
    def fmt(self):
        # identified color format
        if (imtype:=self.identified_type).startswith('palette'):
            return 'INDEX'
        return Image._fmtmap[imtype]

    @property
    def bpp(self):
        # bytes per pixel
        return Image._chlmap[self.fmt]

    @property
    def pixeldepth(self):
        # depth of each pixel
        return self.depth*self.bpp

    def image_get(self):
        if r:=library.MagickGetImage(self.wand):
            return Image(BaseImage(r))
        self.raise_exception()

    def clone(self):
        return Image(image=self)

SUPPORTED_FORMATS=tuple(
    (f,bytes.fromhex('ff'*len(s) if m is None else m),s) for f,m,s in (
        ('BMP',  None,                       b'BM'),
        ('GIF',  None,                       b'GIF87a'),
        ('GIF',  None,                       b'GIF89a'),
        ('PNG',  None,                       b'\x89PNG\r\n\x1a\n'),
        ('JPEG', None,                       b'\xff\xd8\xff'),
        ('JP2',  None,                       b'\x00\x00\x00\x0cjP\x20\x20\x0d\x0a\x87\x0a'),
        ('JP2',  None,                       b'\xffO\xffQ'),
        ('JXL',  None,                       b'\x00\x00\x00\x0cJXL\x20\x0d\x0a\x87\x0a'),
        ('JXL',  None,                       b'\xff\x0a'),
        ('WEBP', 'ffffffff00000000ffffffff', b'RIFF\x00\x00\x00\x00WEBP'),
        ('TIFF', None,                       b'II\x2a\x00'),
        ('TIFF', None,                       b'MM\x00\x2a'),
        ('FLIF', None,                       b'FLIF'),
        ('PDF',  None,                       b'\x25PDF\x2d'),
        ('AVIF', '00000000ffffffffffffffff', b'\x00\x00\x00\x00ftypavif'),
        ('AVIF', '00000000ffffffffffffffff', b'\x00\x00\x00\x00ftypavis'),
        ('HEIF', '00000000ffffffffffffffff', b'\x00\x00\x00\x00ftypmif1'),
        ('HEIF', '00000000ffffffffffffffff', b'\x00\x00\x00\x00ftypmsf1'),
        ('HEIF', '00000000ffffffffffffffff', b'\x00\x00\x00\x00ftypheic'),
        ('HEIF', '00000000ffffffffffffffff', b'\x00\x00\x00\x00ftypheim'),
        ('HEIF', '00000000ffffffffffffffff', b'\x00\x00\x00\x00ftypheis'),
        ('HEIF', '00000000ffffffffffffffff', b'\x00\x00\x00\x00ftypheix'),
        ('HEIF', '00000000ffffffffffffffff', b'\x00\x00\x00\x00ftyphevc'),
        ('HEIF', '00000000ffffffffffffffff', b'\x00\x00\x00\x00ftyphevm'),
        ('HEIF', '00000000ffffffffffffffff', b'\x00\x00\x00\x00ftyphevx'),
    ) if f in Image.supported_formats
)

def getfmt(data):
    for fmt,mask,signature in SUPPORTED_FORMATS:
        if all((m&c)==s for m,c,s in zip(mask,data,signature)):
            return fmt

__all__=['Image','getfmt']


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
