#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from .modules import *
from ..constants import *

__all__=['Canvas','Page','Animation','Thumbnail']

class Canvas(NonWidget):

    __slots__=(
        '_renderer','frameprop','_n_loop','_n_frames',
    )

    def __init__(self,renderer,n_loop=0,n_frames=1,**kwds):
        assert isinstance(n_frames,int),f'n_frames is not integer: {n_frames}'
        self._renderer=renderer
        self.frameprop=None
        self._n_loop=n_loop
        self._n_frames=n_frames
        assert verb(__name__,f'{self}: init {n_frames=}')

    def __bool__(self):
        return not self.closed

    @property
    def renderer(self):
        # return renderer context
        return self._renderer

    @property
    def texture(self):
        # return main texture context
        raise NotImplementedError(f'{self}.texture')

    @property
    def n_loop(self):
        # n_loop of animation, 0 for static image
        return self._n_loop

    @property
    def n_frames(self):
        # n_frames of animation, 1 for static image
        return self._n_frames

    @property
    def size(self):
        # the 'pixels' size of image
        return self.frameprop.size

    @property
    def page_size(self):
        # the 'page' size of image
        return self.frameprop.page_size

    @property
    def offset(self):
        # the 'page' offset of image
        return self.frameprop.offset

    @property
    def has_alpha(self):
        # return True if canvas contains texture with alpha channel
        raise NotImplementedError(f'{self}.has_alpha')

    @property
    def has_page(self):
        # return True if image has 'page' different to 'pixels'
        x,y=self.offset
        return x*y or self.size!=self.page_size

    @property
    def ready(self):
        # return canvas is ready to draw on randerer
        raise NotImplementedError(f'{self}.ready')

    @property
    def closed(self):
        # return canvas is closed
        raise NotImplementedError(f'{self}.closed')

    def add_frame(self,n,pixfmt,frameprop,databuffer):
        # add frame property and data to canvas
        raise NotImplementedError(f'{self}.add_frame')

    @property
    def scale_mode(self):
        # get scaling quality of texture context
        raise NotImplementedError(f'{self}.scale_mode')

    @scale_mode.setter
    def scale_mode(self,value):
        # set scaling quality of texture context
        raise NotImplementedError(f'{self}.scale_mode.setter')

    def is_horizontal(self,use_page=False):
        w,h=self.page_size if use_page else self.size
        return w>h

    def get_canvas_size(self,use_page=False):
        return (*map(sum,zip(self.page_size,self.offset)),) if use_page else (*self.size,)

    def draw(self,srcrect=None,dstrect=None):
        # draw canvas in renderer
        # pass srcrect and dstrect kwds to the texture context
        raise NotImplementedError(f'{self}.draw')

    def create_thumbnail(self,size=256):
        # create and return a thumbnail of this canvas
        assert not self.closed,f'{self} closed'
        assert self.ready,f'{self} not ready'
        return Thumbnail.from_canvas(self,size)

    def current_delay(self):
        # delay in millisecond to show next frame
        pass

    def first_frame(self):
        # set to first frame
        pass

    def next_frame(self):
        # set to next frame, or the first frame if already in last, return new position
        return 0

    def close(self):
        # close canvas
        raise NotImplementedError(f'{self}.close')

class Page(Canvas):

    __slots__=(
        '_texture','_closed',
    )

    def __init__(self,*args,**kwds):
        super().__init__(*args,**kwds)
        # texture in canvas
        self._texture=None
        self._closed=False

    @property
    def texture(self):
        return self._texture

    @property
    def has_alpha(self):
        return self._texture.has_alpha

    @property
    def ready(self):
        return self._texture is not None

    @property
    def closed(self):
        return self._closed

    def add_frame(self,n,pixfmt,frameprop,databuffer):
        assert n==1 and self._texture is None,'Page only support one frame'
        assert trace(__name__,f'{self}: add frame {n=}')
        self.frameprop=frameprop
        self._texture=self.renderer.new_static_texture(pixfmt,frameprop,name=self.name)
        self._texture.update(databuffer,pixfmt)

    @property
    def scale_mode(self):
        if not self.ready:return
        return self._texture.scale_mode

    @scale_mode.setter
    def scale_mode(self,value):
        if not self.ready:return
        if self.scale_mode==value:return
        assert verb(__name__,f'{self}: set scale mode: {value}')
        self._texture.scale_mode=value

    def draw(self,srcrect=None,dstrect=None):
        assert not self.closed,'page closed'
        assert trace(__name__,f'{self}: draw')
        assert (p:=Pref.start(name=f'{__name__}: {self}: draw'))
        self._texture.show(srcrect=srcrect,dstrect=dstrect)
        assert p.record(name=f'draw')
        assert p.log()

    def close(self):
        if self._texture is not None:
            texture=self._texture
            self._texture=None
            texture.close()
        assert verb(__name__,f'{self}: close')
        self._closed=True

class Animation(Canvas):

    __slots__=(
        '_pos','_closed','_textures','_rendered',
    )

    def __init__(self,*args,**kwds):
        super().__init__(*args,**kwds)
        assert self.n_frames>1,f'at least two frames required for Animation'
        self._pos=0
        self._closed=False

        # textures each frame.
        # use list instead of deque for random access
        self._textures=[]
        # index of rendered frame
        self._rendered=set()

    @property
    def texture(self):
        return self._textures[0]

    @property
    def has_alpha(self):
        try:
            return self._textures[0].has_alpha
        except IndexError:
            return False

    @property
    def ready(self):
        return not not self._textures

    @property
    def closed(self):
        return self._closed

    def add_frame(self,n,pixfmt,frameprop,databuffer):
        assert not self.closed,'animation closed'
        assert n-1==len(self._textures),f'wrong frame index: {n} in {len(self._textures)}'
        assert n-1<self.n_frames,f'too many frames: {n}'
        assert trace(__name__,f'{self}: add frame {n=}')
        assert (p:=Pref.start(name=f'{__name__}: {self}: add_frame'))
        if n==1:
            assert self.frameprop is None,'frameprop already set'
            # use size, page_size, offset and pixfmt from first frame
            self.frameprop=frameprop.copy()
        texture=self.renderer.new_static_texture(
            pixfmt,frameprop,name=f'{self.name}[{n-1}]')
        if self._textures:
            texture.scale_mode=self._textures[-1].scale_mode
        assert p.record(name='create frame texture')
        texture.update(databuffer,pixfmt)
        assert p.record(name='update frame texture')
        self._textures.append(texture)
        assert p.record(name='store frame texture')
        assert p.log()

    @property
    def scale_mode(self):
        scale_modes=set(texture.scale_mode for texture in self._textures)
        assert len(scale_modes)==1
        return scale_modes.pop()

    @scale_mode.setter
    def scale_mode(self,value):
        if self.scale_mode==value:return
        assert verb(__name__,f'{self}: set scale mode: {value}')
        for texture in self._textures:
            texture.scale_mode=value

    def draw(self,srcrect=None,dstrect=None):
        assert self._textures,'no frame in animation'
        assert (p:=Pref.start(name=f'{__name__}: {self}: draw'))
        end_pos=pos=min(self._pos,len(self._textures)-1)
        assert trace(__name__,f'{self}: draw {end_pos=}')
        if pos in self._rendered:
            self._textures[pos].show(srcrect=srcrect,dstrect=dstrect)
            assert p.record(name=f'draw')
            assert p.log()
            return
        while pos:
            if self._textures[pos-1].dispose==DISPOSE_BACKGROUND:
                break
            pos-=1
        # always use RGBA32 pixfmt with blend for draft, same size with 1st frame
        assert p.record(name='detect start position')
        draft=self.renderer.new_target_texture(
            SDL_PIXELFORMAT_RGBA32,self.frameprop.copy(),name=f'{self.name}[{end_pos}]')
        draft.set_blend_mode(BLEND_BLEND)
        draft.scale_mode=self.scale_mode
        assert p.record(name='create draft')
        with self.renderer.targetor(draft):
            for texture in self._textures[pos:end_pos]:
                if texture.dispose==DISPOSE_PREVIOUS:
                    continue
                texture.show(dstrect=(*texture.offset,*texture.size))
            texture=self._textures[end_pos]
            texture.show(dstrect=(*texture.offset,*texture.size))
        assert p.record(name='draw in draft')
        draft.show(srcrect=srcrect,dstrect=dstrect)
        assert p.record(name='draw')
        assert p.log()
        draft.delay=texture.delay
        draft.dispose=texture.dispose
        texture.close()
        self._textures[end_pos]=draft
        self._rendered.add(end_pos)
        for pos in range(end_pos-1,0,-1):
            if pos in self._rendered and self._textures[pos].dispose==DISPOSE_NONE:
                self._textures[pos].dispose=DISPOSE_BACKGROUND
                break

    def current_delay(self):
        return self._textures[self._pos].delay

    def first_frame(self):
        assert self._textures,'no frame in animation'
        self._pos=0

    def next_frame(self):
        assert self._textures,'no frame in animation'
        self._pos+=1
        self._pos%=self.n_frames
        self._pos=min(self._pos,len(self._textures)-1)
        return self._pos

    def close(self):
        while self._textures:
            texture=self._textures.pop()
            texture.close()
        assert verb(__name__,f'{self}: close')
        self._closed=True

class Thumbnail(Page):

    __slots__=()

    @classmethod
    def from_canvas(cls,canvas,size):
        instance=cls(canvas.renderer,name=canvas.name)
        assert trace(__name__,f'{instance}: from canvas {canvas}')
        frameprop=canvas.frameprop.copy()
        w,h=frameprop.size
        scale=size/max(w,h)
        if scale<1:
            w,h=int(w*scale),int(h*scale)
            assert trace(__name__,f'{instance}: downscale',
                         f'{frameprop.width}x{frameprop.height}->{w}x{h}')
        frameprop.size=w,h
        instance.frameprop=frameprop
        texture=instance.renderer.new_target_texture(
            SDL_PIXELFORMAT_RGBA32,instance.frameprop,name=f'(thumbnail){canvas.name}')
        texture.set_blend_mode(BLEND_BLEND)
        instance._texture=texture
        texture.scale_mode=canvas.scale_mode
        with instance.renderer.targetor(texture):
            canvas.texture.show(dstrect=(0,0,w,h))
        return instance

    def create_thumbnail(self):
        return self


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
