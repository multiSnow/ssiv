#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from ..co import OrderedDict

__all__=['Containers']

def _relocate(lst,pos,step):
    # change from current position with step from pos in lst, return new position
    assert isinstance(step,int)
    if not (lst and step):return 0
    pos+=step
    return min(max(0,pos),len(lst)-1)

class Containers(OrderedDict):

    __slots__=(
        '_pos','_fnpos',
        '_sortkey','_revsort',
    )

    def __init__(self,key=None,reverse=False):
        self._pos=0
        self._fnpos=0
        self._sortkey=key
        self._revsort=not not reverse

    def __setitem__(self,key,value):
        if key not in self:
            super().__setitem__(key,[])
        self[key][:]=sorted(value,key=self.sortkey,reverse=self.revsort)

    def _resort(self):
        # re-sort filenames and re-index filename
        container,filename=self.current_page
        for filenames in self.values():
            filenames.sort(key=self.sortkey,reverse=self.revsort)
        self._fnpos=self[container].index(filename)

    @property
    def sortkey(self):
        # 'key' argument that used in sorting filenames
        return self._sortkey

    @sortkey.setter
    def sortkey(self,value):
        if value is self.sortkey:return
        self._sortkey=value
        self._resort()

    @property
    def revsort(self):
        # 'reverse' argument that used in sorting filenames
        return self._revsort

    @revsort.setter
    def revsort(self,value):
        if (not not value) is self.revsort:return
        self._revsort=not self._revsort
        self._resort()

    @property
    def current_page(self):
        # current container and filename
        if not self:return
        while True:
            try:
                container,filenames=self.getitem(self._pos)
            except IndexError:
                if not self.cross(-1):
                    return
            while True:
                try:
                    return container,filenames[self._fnpos]
                except IndexError:
                    if not self.flip(-1):
                        break

    @property
    def current_position(self):
        # current position of container and filename
        if self.current_page is None:return
        return self._pos,self._fnpos

    @property
    def container_length(self):
        # length of current container
        if self.current_page is None:return
        return len(self.getitem(self._pos)[1])

    def flip(self,step=1,/):
        # flip filename position with step, default is flip to next filename
        # return True if filename position changed
        assert isinstance(step,int)
        if not step:return
        oldpos=self._fnpos
        try:
            filenames=self.getvalue(self._pos)
        except IndexError:
            return self.cross(-1)
        else:
            self._fnpos=_relocate(filenames,self._fnpos,step)
        return self._fnpos!=oldpos

    def cross(self,step=1,/):
        # go cross container with step, default is cross to next container
        # return True if container position changed
        # reset filename position to 0
        assert isinstance(step,int)
        if not step:return
        oldpos=self._pos
        self._pos=_relocate(self,self._pos,step)
        if oldpos==self._pos:return False
        self._fnpos=0
        return True

    def remove_filename(self,container,filename):
        # remove filename from container
        # container will also be removed if it becomes empty
        try:
            self[container].remove(filename)
            if not self[container]:
                del self[container]
        except (KeyError,IndexError):
            pass
        return not not self


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
