#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from .hci_action import *
from .modules import *
from ..co import AttrDict

__all__=['HCI']

class HCI(NonWidget):

    __slots__=(
        '_env','_acted','_pressed_keys','_pressed_keys_ops','_pressed_mousebtns',
        '_kbdglobal','_kbdtiling','_kbdcanvas','_kbdmotion',
        '_mouseclck','_mousemask','_wheelmask','_mouserev','_wheelrev','_faststep',
    )

    _ctrls=frozenset(f'SDLK_{s}CTRL' for s in 'LR')
    _alts=frozenset(f'SDLK_{s}ALT' for s in 'LR')
    _shifts=frozenset(f'SDLK_{s}SHIFT' for s in 'LR')

    def __init__(self,env,config):
        self._env=env
        self._acted=False
        self._kbdglobal=self._config_to_map(config.kbd_global)
        self._kbdtiling=self._config_to_map(config.kbd_tiling)
        self._kbdcanvas=self._config_to_map(config.kbd_canvas)
        self._kbdmotion=self._config_to_map(config.kbd_motion)
        self._mouseclck=self._config_to_map(config.mouse_clck)
        self._mousemask=self._config_to_map(config.mouse_mask)
        self._wheelmask=self._config_to_map(config.wheel_mask)
        self._mouserev=(
            (-1 if config.mouse_dire.revxmtn else 1),
            (-1 if config.mouse_dire.revymtn else 1),
        )
        self._wheelrev=(
            (-1 if config.mouse_dire.revxwhl else 1),
            (-1 if config.mouse_dire.revywhl else 1),
        )
        # names of keycode currently pressed
        self._pressed_keys=set()
        self._pressed_keys_ops={
            SDL_PRESSED:self._pressed_keys.add,
            SDL_RELEASED:self._pressed_keys.discard,
        }
        # {button_name:{clicks:int, x:int, y:int}}
        self._pressed_mousebtns={}
        # pixels for fast move
        self._faststep=config.user_interface.faststep

    def _config_to_map(self,config):
        bindings_map={}
        for action in config:
            for bindings in config[action]:
                if isinstance(bindings,str):
                    bindings_map[bindings]=action
                else:
                    bindings_map[frozenset(bindings)]=action
        return bindings_map

    def _combine_lr(self,keys):
        keys=set(keys)
        if keys&self._ctrls:
            keys-=self._ctrls
            keys.add('SDLK_CTRL')
        if keys&self._alts:
            keys-=self._alts
            keys.add('SDLK_ALT')
        if keys&self._shifts:
            keys-=self._shifts
            keys.add('SDLK_SHIFT')
        return frozenset(keys)

    def _match_keys(self,kbd,keys):
        return kbd.get(keys,False) or kbd.get(self._combine_lr(keys),False)

    def handle_key_event(self,key):
        assert is_mainthread()
        if key.repeat:return
        keysym=SDLE_GetKeycodeName(key.keysym.sym)
        assert trace(__name__,f'{self}: keyevent {keysym}',
                     'pressed' if key.state==SDL_PRESSED else 'released')
        keys=frozenset(self._pressed_keys)
        self._env.action_motion_stop()
        if key.state==SDL_PRESSED:
            self._acted=False
        if not self._acted and key.state==SDL_RELEASED:
            viewmode=None
            if not (action:=self._match_keys(self._kbdglobal,keys)):
                viewmode=self._env.viewmode
                kbd=self._kbdtiling if viewmode==SDL_SDLUI_VIEWTILING else self._kbdcanvas
                action=self._match_keys(kbd,keys)
            if action:
                assert verb(__name__,f'{self}: match key action',
                            'global' if viewmode is None else \
                            ('tiling' if viewmode==SDL_SDLUI_VIEWTILING else 'canvas'),
                            action,','.join(sorted(keys)))
                self._acted=True
                execute_hci_action(viewmode,action,self._env)
        self._pressed_keys_ops[key.state](keysym)
        if not self._pressed_keys:
            self._acted=False
        elif self._env.viewmode==SDL_SDLUI_VIEWCANVAS:
            if action:=self._match_keys(self._kbdmotion,frozenset(self._pressed_keys)):
                execute_hci_action(SDL_SDLUI_VIEWCANVAS,action,self._env,
                                   AttrDict(step=self._faststep))

    def handle_mousebtn_event(self,btn):
        assert is_mainthread()
        if btn.which==SDL_TOUCH_MOUSEID:return
        self._env.action_motion_stop()
        name=SDLE_GetButtonName(btn.button)
        assert trace(__name__,f'{self}: mouse button event',
                     name,btn.clicks,btn.timestamp,
                     'pressed' if btn.state==SDL_PRESSED else 'released')
        if self._env.viewmode!=SDL_SDLUI_VIEWCANVAS:return
        offx,offy,w,h=self._env.get_renderer_area_for_canvas()
        x,y=(btn.x-offx,btn.y-offy)
        if btn.state==SDL_PRESSED:
            if min(x,y,w-x,h-y)<0:return
            self._pressed_mousebtns.setdefault(name,{}).update(
                clicks=btn.clicks,x=x,y=y,
            )
            runsdl(SDL_SetRelativeMouseMode,SDL_TRUE)
            return
        if self._pressed_mousebtns.pop(name,None) is None:return
        runsdl(SDL_SetRelativeMouseMode,SDL_FALSE)
        if min(x,y,w-x,h-y)<0:return
        if self._pressed_mousebtns:return
        if self._acted:
            self._acted=False
            return
        if not (action:=self._match_keys(self._mouseclck,name)):return
        assert verb(__name__,f'{self}: match button action',action,name,
                    f'{x},{y} in {w}x{h}')
        execute_hci_action(SDL_SDLUI_VIEWCANVAS,action,self._env,
                           AttrDict(zip('xywh',(x,y,w,h))))

    def handle_mousemtn_event(self,mtn):
        assert is_mainthread()
        if mtn.which==SDL_TOUCH_MOUSEID:return
        if self._env.viewmode!=SDL_SDLUI_VIEWCANVAS:return
        if len(self._pressed_mousebtns)!=1:return
        if self._acted:return
        name=set(self._pressed_mousebtns).pop()
        stat=self._pressed_mousebtns[name]
        btns=frozenset((name,stat['clicks']))|self._pressed_keys
        if not (action:=self._match_keys(self._mousemask,btns)):return
        offx,offy,w,h=self._env.get_renderer_area_for_canvas()
        x,y=max(0,min(w,mtn.x-offx)),max(0,min(h,mtn.y-offy))
        execute_hci_action(SDL_SDLUI_VIEWCANVAS,action,self._env,
                           AttrDict(pos=(x-stat['x'],y-stat['y']),
                                    relpos=(mtn.xrel,mtn.yrel),
                                    rev=self._mouserev))

    def handle_mousewhl_event(self,whl):
        assert is_mainthread()
        if whl.which==SDL_TOUCH_MOUSEID:return
        if self._env.viewmode!=SDL_SDLUI_VIEWCANVAS:return
        statuses=frozenset(self._pressed_keys)|frozenset(self._pressed_mousebtns)
        if not statuses:
            action='whlmove'
        elif action:=self._match_keys(self._wheelmask,statuses):
            self._acted=True
        else:
            return
        m=-1 if whl.direction==SDL_MOUSEWHEEL_FLIPPED else 1
        execute_hci_action(SDL_SDLUI_VIEWCANVAS,action,self._env,
                           AttrDict(pos=(whl.x*m,whl.y*m),
                                    step=self._faststep,
                                    rev=self._wheelrev))

    def handle_touchact_event(self,act):
        assert is_mainthread()
        # TODO: act.touchId, act.fingerId, (act.x,act.y), (act.dx,act.dy), act.pressure


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
