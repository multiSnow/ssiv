#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from functools import reduce
from math import ceil
from operator import mul

from .modules import *

__all__=['execute_hci_action']

def mulreduce(*args):
    return reduce(mul,args,1)

def slowdown(n):
    m=-1 if n<0 else 1
    return ceil(n*m/10)*m

def execute_hci_action(mode,action,env,args=None):
    match (mode,action):

        # global
        case (None,'quit'):
            env.action_quit()
        case (None,'clear'):
            env.clear()
        case (None,'rtl'):
            env.double_right=not env.double_right
        case (None,'fullscreen'):
            env.window.fullscreen=not env.window.fullscreen
        case (None,'tiling'):
            env.action_view_tiling()
        case (None,'canvas'):
            env.action_view_canvas()
        case (None,'checker'):
            env.toggle_checker()
        case (None,'showsb'):
            env.toggle_statusbar()
        case (None,'rotesb'):
            env.rotate_statsbar()
        case (_,'crossnext'):
            env.action_cross(1)
        case (_,'crossprev'):
            env.action_cross(-1)

        # tiling mode
        case (SDLE_EvEmu.SDL_SDLUI_VIEWTILING,'selectup'):
            if env.containers.flip(-1 if env.double_right else -env.tiling_width):
                env.action_view_tiling()
        case (SDLE_EvEmu.SDL_SDLUI_VIEWTILING,'selectdown'):
            if env.containers.flip( 1 if env.double_right else  env.tiling_width):
                env.action_view_tiling()
        case (SDLE_EvEmu.SDL_SDLUI_VIEWTILING,'selectleft'):
            if env.containers.flip( env.tiling_width if env.double_right else -1):
                env.action_view_tiling()
        case (SDLE_EvEmu.SDL_SDLUI_VIEWTILING,'selectright'):
            if env.containers.flip(-env.tiling_width if env.double_right else  1):
                env.action_view_tiling()
        case (SDLE_EvEmu.SDL_SDLUI_VIEWTILING,'selectfirst'):
            while env.containers.flip(-1):pass
            env.action_view_tiling()
        case (SDLE_EvEmu.SDL_SDLUI_VIEWTILING,'selectlast'):
            while env.containers.flip( 1):pass
            env.action_view_tiling()
        case (SDLE_EvEmu.SDL_SDLUI_VIEWTILING,'scaleup'):
            thumbsize=env.thumbsize
            thumbsize*=2
            thumbsize=int(min(max(16,thumbsize),env.maxthumbsize))
            env.thumbsize=thumbsize
        case (SDLE_EvEmu.SDL_SDLUI_VIEWTILING,'scaledown'):
            thumbsize=env.thumbsize
            thumbsize/=2
            thumbsize=int(min(max(16,thumbsize),env.maxthumbsize))
            env.thumbsize=thumbsize

        # canvas mode
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'antialias'):
            env.anti_alias=not env.anti_alias
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'usepage'):
            env.use_page=not env.use_page
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'double'):
            env.double_canvas=not env.double_canvas
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'scaleup'):
            env.scale_up()
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'scaledown'):
            env.scale_down()
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'autofit'):
            env.scale_ratio=0
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'flipnext'):
            if env.containers.flip((env.double_canvas and env.scene_len>1)+1):
                env.action_view_canvas()
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'flipprev'):
            if env.containers.flip(-((env.double_canvas and env.scene_len>1)+1)):
                env.action_view_canvas()
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'flipnextharf'):
            if env.containers.flip(1):
                env.action_view_canvas()
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'flipprevharf'):
            if env.containers.flip(-1):
                env.action_view_canvas()
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'flipfirst'):
            if env.containers.flip(-1):
                while env.containers.flip(-1):pass
                env.action_view_canvas()
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'fliplast'):
            if env.containers.flip( 1):
                while env.containers.flip( 1):pass
                env.action_view_canvas()

        # keyboard motion in canvas mode
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewup'):
            env.action_motion_start(y=-1)
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewdown'):
            env.action_motion_start(y= 1)
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewleft'):
            env.action_motion_start(x=-1)
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewright'):
            env.action_motion_start(x= 1)
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewlu'):
            env.action_motion_start(x=-1,y=-1)
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewru'):
            env.action_motion_start(x= 1,y=-1)
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewld'):
            env.action_motion_start(x=-1,y= 1)
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewrd'):
            env.action_motion_start(x= 1,y= 1)

        # keyboard motion in canvas mode
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewfastup'):
            env.action_motion_start(y=-args.step)
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewfastdown'):
            env.action_motion_start(y= args.step)
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewfastleft'):
            env.action_motion_start(x=-args.step)
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewfastright'):
            env.action_motion_start(x= args.step)
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewfastlu'):
            env.action_motion_start(x=-args.step,y=-args.step)
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewfastru'):
            env.action_motion_start(x= args.step,y=-args.step)
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewfastld'):
            env.action_motion_start(x=-args.step,y= args.step)
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'viewfastrd'):
            env.action_motion_start(x= args.step,y= args.step)

        # click in canvas mode
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'clickflip'):
            if env.containers.flip((-1 if args.x<args.w/2 else 1)*\
                                   (-1 if env.double_right else 1)):
                env.action_view_canvas()

        # mouse motion in canvas mode
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'mtnpath'):
            env.canvas_move(*map(mul,args.relpos,args.rev))
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'mtnvector'):
            env.action_motion_start(
                **dict(zip('xy',map(
                    slowdown,map(mul,args.pos,args.rev)))))

        # wheel in canvas mode
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'whlmove'):
            env.canvas_move(*map(mul,args.pos,args.rev))
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'whlfastmove'):
            env.canvas_move(*map(mulreduce,args.pos,args.rev,
                                 (args.step,args.step)))
        case (SDLE_EvEmu.SDL_SDLUI_VIEWCANVAS,'whlscale'):
            if scale:=sum(map(mul,args.pos,args.rev)):
                (env.scale_up if scale>0 else env.scale_down)()

        case _:
            assert warn(__name__,f'unknown mode or action: {mode},{action}')

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
