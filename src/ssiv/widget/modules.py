#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from os import urandom
from sys import exc_info

from .. import sdl2e
from .. import tools

__all__=[
    'false','false0','false1','falseuint',
    'runsdl','runsdlP','SDLError','NonWidget','Widget',
    *sdl2e.__all__,*tools.__all__,
]

globals().update((attr,getattr(sdl2e,attr)) for attr in sdl2e.__all__)
globals().update((attr,getattr(tools,attr)) for attr in tools.__all__)

class SDLError(Exception):

    __slots__=('funcname','result','message')

    def __init__(self,name,result):
        self.funcname=name
        self.result=result
        self.message=SDL_GetError()
    def __str__(self):
        return f'{self.funcname} ({self.result}) ({bytes2str(self.message)})'

class NonWidget:
    __slots__=('_name','_uid')
    def __new__(cls,*args,name=None,**kwds):
        instance=super().__new__(cls)
        instance._uid=urandom(256).__hash__()
        instance._name=name
        return instance
    def __init__(self,*args,**kwds):pass
    def __hash__(self):
        return self._uid
    def __eq__(self,other):
        return self.__hash__()==other.__hash__()
    def __repr__(self):
        return self.__class__.__name__ \
            if self.name is None else \
            f'{self.__class__.__name__}({self.name})'
    @property
    def name(self):
        return self._name

class Widget(NonWidget):
    __slots__=('_context',)
    def __new__(cls,*args,**kwds):
        instance=super().__new__(cls,*args,**kwds)
        instance._context=None
        return instance
    def __init__(self,*args,**kwds):pass
    @property
    def real(self):
        return self._context

def false(rc):pass
def false0(rc):return rc
def false1(rc):return not rc
def falseuint(rc):return rc<0

def _runner(func,*args):
    return func(*(arg.real if isinstance(arg,Widget) else arg for arg in args))

def runsdl(func,*args,errval=false0):
    assert is_mainthread()
    rc=_runner(func,*args)
    if callable(errval):
        if errval(rc):
            raise SDLError(func.__name__,rc)
    elif rc==errval:
        raise SDLError(func.__name__,rc)
    return rc

def runsdlP(func,*args):
    assert is_mainthread()
    try:
        return _runner(func,*args).contents
    except ValueError:
        tb=exc_info()[2]
    raise SDLError(func.__name__,None).with_traceback(tb)


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
