#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from ctypes import c_uint8,c_int

from .canvas import *
from .modules import *
from .texture import *

__all__=['Renderer']

class Renderer(Widget):

    __slots__=(
        '_renderer',
        '_renderer_info',
        '_driver',
        '_is_software',
        '_vsync',
        '_texture_formats',
        '_max_texture_width',
        '_max_texture_height',
        '_output_w',
        '_output_h',
        '_logical_w',
        '_logical_h',
        '_cue',
    )

    def __init__(self,window=None):
        self._context=None

        self._renderer_info=SDL_RendererInfo()

        if window is not None:
            self.setup(window)

    def setup(self,window):
        self.close()
        self._context=window.new_renderer()
        assert trace(__name__,f'{self}: setup')

    def _init_size(self):
        self._output_w=-1
        self._output_h=-1
        self._logical_w=-1
        self._logical_h=-1

    def _get_renderer_info(self):
        runsdl(SDL_GetRendererInfo,self,self._renderer_info)
        self._driver=bytes2str(self._renderer_info.name)
        self._is_software=not not self._renderer_info.flags&SDL_RENDERER_SOFTWARE
        self._vsync=not not self._renderer_info.flags&SDL_RENDERER_PRESENTVSYNC
        self._texture_formats=tuple(filter(None,self._renderer_info.texture_formats))
        self._max_texture_width=self._renderer_info.max_texture_width
        self._max_texture_height=self._renderer_info.max_texture_height

    def _get_output_size(self):
        runsdl(SDL_GetRendererOutputSize,self,w:=c_int(),h:=c_int())
        self._output_w=w.value
        self._output_h=h.value

    def _get_logical_size(self):
        runsdl(SDL_RenderGetLogicalSize,self,w:=c_int(),h:=c_int())
        self._logical_w=w.value or -1
        self._logical_h=h.value or -1

    @property
    def driver(self):
        if self._driver is None:
            self._get_renderer_info()
        return self._driver

    @property
    def vsync(self):
        if self._vsync is None:
            self._get_renderer_info()
        return self._vsync

    @property
    def is_software(self):
        if self._is_software is None:
            self._get_renderer_info()
        return self._is_software

    @property
    def texture_formats(self):
        if self._texture_formats is None:
            self._get_renderer_info()
        return self._texture_formats

    @property
    def max_texture_width(self):
        if self._max_texture_width is None:
            self._get_renderer_info()
        return self._max_texture_width

    @property
    def max_texture_height(self):
        if self._max_texture_height is None:
            self._get_renderer_info()
        return self._max_texture_height

    @property
    def output_width(self):
        if self._output_w<0:
            self._get_output_size()
        return self._output_w

    @property
    def output_height(self):
        if self._output_h<0:
            self._get_output_size()
        return self._output_h

    @property
    def output_size(self):
        return self.output_width,self.output_height

    @property
    def width(self):
        if self._logical_w<0:
            self._get_logical_size()
        return self._logical_w

    @width.setter
    def width(self,value):
        if self.width==value:return
        self._logical_w=-1
        return runsdl(SDL_RenderSetLogicalSize,self,value,self.height)

    @property
    def height(self):
        if self._logical_h<0:
            self._get_logical_size()
        return self._logical_h

    @height.setter
    def height(self,value):
        if self.height==value:return
        self._logical_h=-1
        return runsdl(SDL_RenderSetLogicalSize,self,self.width,value)

    @property
    def size(self):
        return self.width,self.height

    @size.setter
    def size(self,value):
        w,h=value
        if self.size==(w,h):return
        self._logical_w=self._logical_h=-1
        runsdl(SDL_RenderSetLogicalSize,self,*value)

    @property
    def viewport(self):
        runsdl(SDL_RenderGetViewport,self,rect:=SDL_Rect())
        return rect.x,rect.y,rect.w,rect.h

    @viewport.setter
    def viewport(self,value):
        runsdl(SDL_RenderSetViewport,self,SDL_Rect(*value))

    def viewporter(self,viewport):
        return RenderViewporter(self,viewport)

    def get_draw_color(self):
        runsdl(SDL_GetRenderDrawColor,self,
               r:=c_uint8(),g:=c_uint8(),b:=c_uint8(),a:=c_uint8())
        return r.value,g.value,b.value,a.value

    def set_draw_color(self,r,g,b,a=255,/):
        assert all((
            isinstance(r,int),-1<r<256,isinstance(g,int),-1<g<256,
            isinstance(b,int),-1<b<256,isinstance(a,int),-1<a<256,
        ))
        return runsdl(SDL_SetRenderDrawColor,self,r,g,b,a)

    def drawer(self,color=None):
        return RendererDrawer(self,color)

    @property
    def blend_mode(self):
        runsdl(SDL_GetRenderDrawBlendMode,self,mode:=SDL_BlendMode())
        return mode.value

    @blend_mode.setter
    def blend_mode(self,value):
        runsdl(SDL_SetRenderDrawBlendMode,self,value)

    def blender(self,blend=None):
        return RendererBlender(self,blend)

    def new_canvas(self,n_loop=0,n_frames=1,pieces=1,name=None):
        if n_frames>1:
            canvas=Animation(self,n_loop=n_loop,n_frames=n_frames,name=name)
        else:
            canvas=Page(self,name=name)
        return canvas

    def new_thumbnail(self,name=None):
        return Thumbnail(self,name=name)

    def new_texture(self,pixfmt,width,height,access):
        return runsdlP(SDL_CreateTexture,self,pixfmt,access,width,height)

    def new_static_texture(self,pixfmt,frameprop,name=None):
        return Texture(self,pixfmt,frameprop,access='static',name=name)

    def new_stream_texture(self,pixfmt,frameprop,name=None):
        return Texture(self,pixfmt,frameprop,access='stream',name=name)

    def new_target_texture(self,pixfmt,frameprop,name=None):
        return TargetTexture(self,pixfmt,frameprop,name=name)

    def get_target(self):
        return runsdlP(SDL_GetRenderTarget,self)

    def set_target(self,target=None):
        runsdl(SDL_SetRenderTarget,self,target)
        if target is not None:
            self.clear() # clear target

    def targetor(self,target):
        return RenderTargetor(self,target)

    def clear(self):
        self._init_size()
        assert trace(__name__,f'{self}: clear')
        return runsdl(SDL_RenderClear,self)

    def present(self):
        assert trace(__name__,f'{self}: present')
        return runsdl(SDL_RenderPresent,self)

    @property
    def available_pixfmts(self):
        yield 'available renderer pixel formats:'
        yield from (SDLE_GetPixFmtName(pixfmt)
                    for pixfmt in self.texture_formats)

    def destroy(self):
        if self._context is not None:
            renderer=self._context
            self._context=None
            runsdl(SDL_DestroyRenderer,renderer)
            assert trace(__name__,f'{self}: close')

        self._driver=None
        self._is_software=None
        self._vsync=None
        self._texture_formats=None
        self._max_texture_width=None
        self._max_texture_height=None
        self._init_size()

    def close(self):
        self.destroy()

class RenderTargetor(NonWidget):

    __slots__=(
        '_renderer','_target','_previous_target',
    )

    def __init__(self,renderer,target):
        self._renderer=renderer
        self._target=target

    def __enter__(self):
        try:
            self._previous_target=self._renderer.get_target()
        except SDLError:
            self._previous_target=None
        self._renderer.set_target(self._target)
        assert trace(__name__,f'{self}: renderer to {self._target}')

    def __exit__(self,exc_type,exc_value,exc_tb):
        self._renderer.set_target(self._previous_target)
        assert trace(__name__,f'{self}: renderer to',
                     'window' if self._previous_target is None else self._previous_target)

class RenderViewporter(NonWidget):

    __slots__=(
        '_renderer','_viewport','_previous_viewport',
    )

    def __init__(self,renderer,viewport):
        self._renderer=renderer
        self._viewport=viewport

    def __enter__(self):
        self._previous_viewport=self._renderer.viewport
        if self._viewport is not None:
            self._renderer.viewport=self._viewport
            assert trace(__name__,f'{self}: enter viewporter',
                         prettyrect(self._renderer.viewport))
        else:
            assert trace(__name__,f'{self}: enter viewporter')

    def __exit__(self,exc_type,exc_value,exc_tb):
        self._renderer.viewport=self._previous_viewport
        assert trace(__name__,f'{self}: exit viewporter',
                     prettyrect(self._renderer.viewport))

class RendererDrawer(NonWidget):

    __slots__=(
        '_renderer','_color','_previous_color',
    )

    def __init__(self,renderer,color):
        self._renderer=renderer
        self._color=color

    def __enter__(self):
        self._previous_color=self._renderer.get_draw_color()
        if self._color is not None:
            self._renderer.set_draw_color(*self._color)
            assert trace(__name__,f'{self}: enter drawer',
                         prettycolor(self._color))
        else:
            assert trace(__name__,f'{self}: enter drawer')

    def __exit__(self,exc_type,exc_value,exc_tb):
        self._renderer.set_draw_color(*self._previous_color)
        assert trace(__name__,f'{self}: exit drawer',
                    prettycolor(self._previous_color))

class RendererBlender(NonWidget):

    __slots__=(
        '_renderer','_blend','_previous_blend',
    )

    def __init__(self,renderer,blend):
        self._renderer=renderer
        self._blend=blend or SDL_BLENDMODE_NONE

    def __enter__(self):
        self._previous_blend=self._renderer.blend_mode
        self._renderer.blend_mode=self._blend
        assert trace(__name__,f'{self}: enter blender',self._blend)

    def __exit__(self,exc_type,exc_value,exc_tb):
        self._renderer.blend_mode=self._previous_blend
        assert trace(__name__,f'{self}: exit blender',self._previous_blend)


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
