#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from .environ import *
from .modules import *

__all__=['SubSystem','VideoSubSystem']

class SubSystem(NonWidget):

    __slots__=('flags','env','config')
    _flags_={
        'timer':SDL_INIT_TIMER,
        'audio':SDL_INIT_AUDIO,
        'video':SDL_INIT_VIDEO,
        # use SDL_INIT_GAMECONTROLLER instead of SDL_INIT_JOYSTICK
        # use SDL_INIT_GAMECONTROLLER also enable SDL_INIT_HAPTIC
        'gamepad':SDL_INIT_GAMECONTROLLER|SDL_INIT_HAPTIC,
        'event':SDL_INIT_EVENTS,
        'all':SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_GAMECONTROLLER|SDL_INIT_HAPTIC,
    }

    @classmethod
    def new(cls,config,**kwds):
        flags=0
        system=[]
        for k,v in kwds.items():
            assert k in cls._flags_,f'unknown SubSystem: {k}'
            if v:
                flags|=cls._flags_[k]
                system.append(k)
        instance=cls(name=','.join(system))
        instance.flags=flags
        instance.config=config
        return instance

    def __enter__(self):
        runsdl(SDL_Init,self.flags)
        self.env=ENV(self.config)
        assert trace(__name__,f'{self}: start')
        return self.env

    def __exit__(self,exc_type,exc_value,exc_traceback):
        runsdl(SDL_Quit)
        self.env.clear()
        assert trace(__name__,f'{self}: exit')

def VideoSubSystem(config):
    return SubSystem.new(config,video=True,event=True,timer=True)

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
