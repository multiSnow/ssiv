#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from io import BytesIO

from .modules import *
from ..ct import c_int,c_uint,c_void_p,bytearray2void
from ..image_loader_frame import generate_crop_points,DataBuffer

__all__=['StaticTexture','StreamTexture','TargetTexture','Texture']

class TextureMixIn:
    __slots__=()

    @property
    def renderer(self):
        return self._renderer

    @property
    def has_alpha(self):
        return not not SDL_ISPIXELFORMAT_ALPHA(self.pixfmt)

    @property
    def pixfmt(self):
        return self._pixfmt

    @property
    def delay(self):
        return self._frameprop.delay

    @delay.setter
    def delay(self,value):
        self._frameprop.delay=value

    @property
    def dispose(self):
        return self._frameprop.dispose

    @dispose.setter
    def dispose(self,value):
        self._frameprop.dispose=value

    @property
    def blend(self):
        return self._frameprop.blend

    @blend.setter
    def blend(self,value):
        self._frameprop.blend=value

    @property
    def bpp(self):
        return SDL_BYTESPERPIXEL(self.pixfmt)

    @property
    def width(self):
        return self._frameprop.width

    @property
    def height(self):
        return self._frameprop.height

    @property
    def size(self):
        return self.width,self.height

    @property
    def offset_x(self):
        return self._frameprop.offset_x

    @property
    def offset_y(self):
        return self._frameprop.offset_y

    @property
    def offset(self):
        return self._frameprop.offset

class BaseTexture(Widget,TextureMixIn):

    __slots__=(
        '_frameprop','_pixfmt','_renderer','_scale_mode','_bool',
    )

    # there is no real 'best' (anisotropic filtering), even for Direct3D.
    # see *_CreateTexture of each SDL_render_*.c
    _scale_mode_str2emu={
        'nearest':SDL_ScaleModeNearest,
        'linear':SDL_ScaleModeLinear,
    }
    _scale_mode_emu2str={v:k for k,v in _scale_mode_str2emu.items()}

    def __init__(self,renderer,pixfmt,frameprop,access=None,**kwds):
        assert access in (
            SDL_TEXTUREACCESS_STATIC,
            SDL_TEXTUREACCESS_STREAMING,
            SDL_TEXTUREACCESS_TARGET,
        ),f'invalid access: {access}'
        self._frameprop=frameprop
        self._pixfmt=pixfmt
        self._renderer=renderer
        self._scale_mode=None
        self._bool=True

        self._context=renderer.new_texture(pixfmt,*frameprop.size,access)
        runsdl(SDL_QueryTexture,self,pixfmt:=c_uint(),None,None,None)
        assert self.pixfmt==pixfmt.value,f'unsupported pixfmt: {pixfmt}'
        assert trace(__name__,f'{self}: setup',
                     SDLE_GetPixFmtName(pixfmt.value),
                     '{}x{}'.format(*frameprop.size))
        self.set_blend_mode(frameprop.blend)

    def __bool__(self):
        return not not self._bool

    @property
    def scale_mode(self):
        if self._scale_mode is None:
            runsdl(SDL_GetTextureScaleMode,self,(mode:=c_int()))
            self._scale_mode=self._scale_mode_emu2str[mode.value]
        return self._scale_mode

    @scale_mode.setter
    def scale_mode(self,value):
        assert value in self._scale_mode_str2emu,f'invalid scale mode: {value}'
        if value==self.scale_mode:return # no change needed
        self._scale_mode=None
        assert trace(__name__,f'{self}: set scale mode: {value}')
        runsdl(SDL_SetTextureScaleMode,self,self._scale_mode_str2emu[value])

    def set_blend_mode(self,blend):
        runsdl(SDL_SetTextureBlendMode,self,blend)
        self.blend=blend

    def show(self,srcrect=None,dstrect=None):
        src=(0,0,*self.size) if srcrect is None else (*srcrect,)
        dst=(0,0,*self.renderer.size) if dstrect is None else (*dstrect,)
        runsdl(SDL_RenderCopy,self.renderer,self,SDL_Rect(*src),SDL_Rect(*dst))
        assert trace(__name__,f'{self}: show',
                     f'{prettyrect(src)}->{prettyrect(dst)}')

    def destroy(self):
        self._bool=False
        runsdl(SDL_DestroyTexture,self)
        assert trace(__name__,f'{self}: close')

    def close(self):
        return self.destroy()

    def update(self,databuffer,pixfmt):
        raise NotImplementedError(f'{self}.update')

class StaticTexture(BaseTexture):

    __slots__=()

    def __init__(self,*args,**kwds):
        super().__init__(*args,access=SDL_TEXTUREACCESS_STATIC,**kwds)

    def update(self,databuffer,pixfmt):
        # NOTE: only SDL_UpdateTexture on static texture does not need padding
        #       (align by SDL_UpdateTextureNative)
        #       (see SDL_render.c in SDL source code)
        assert pixfmt==self.pixfmt,f'update with different pixfmt is not support'
        assert (p:=Pref.start(name=f'{self}: update'))
        data=databuffer.pixels
        databuffer.pixels=None
        assert trace(__name__,f'{self}: update start')
        runsdl(SDL_UpdateTexture,self,None,bytearray2void(data),self.width*self.bpp)
        assert trace(__name__,f'{self}: update finished')
        assert p.record(name='update')
        data.clear()
        assert p.record(name='clear')
        assert p.log()

class StreamTexture(BaseTexture):

    __slots__=(
        '_lockpixel',
        '_lockpitch',
        '_locked_w',
        '_locked_h',
        '_locked_pitch',
        '_pitch_aligned',
    )

    def __init__(self,*args,**kwds):
        super().__init__(*args,access=SDL_TEXTUREACCESS_STREAMING,**kwds)
        self._lockpixel=c_void_p()
        self._lockpitch=c_int()
        self._locked_w=-1
        self._locked_h=-1
        self._locked_pitch=-1
        self._pitch_aligned=False

    def locker(self,rect=None):
        return TextureLocker(self,rect)

    def lock(self,rect):
        assert self._locked_w<0 and self._locked_h<0 and self._locked_pitch<0,\
            'already locked'
        if rect is not None:
            x,y,w,h=rect
            rect=SDL_Rect(x,y,w,h)
            width=w
            height=h
        else:
            width=self.width
            height=self.height
        self._locked_w=width
        self._locked_h=height

        runsdl(SDL_LockTexture,self,rect,self._lockpixel,self._lockpitch)
        assert trace(__name__,f'{self}: lock {rect.w}x{rect.h}+{rect.x}+{rect.y}')

        pitch=self._locked_w*self.bpp
        if height>1 and (padding:=(4-pitch)%4):
            pitch+=padding
            self._pitch_aligned=True
        self._locked_pitch=pitch

    def unlock(self):
        assert self._locked_w>0 and self._locked_h>0 and self._locked_pitch>0,'not locked'
        self._locked_w=-1
        self._locked_h=-1
        self._locked_pitch=-1
        self._pitch_aligned=False

        runsdl(SDL_UnlockTexture,self)
        assert trace(__name__,f'{self}: unlock')

    def stream(self,pixels,pixfmt):
        assert min(self._locked_w,self._locked_h,self._locked_pitch)>0,'not locaked'
        if pixfmt!=self.pixfmt or self._pitch_aligned:
            runsdl(SDL_ConvertPixels,self._locked_w,self._locked_h,
                   pixfmt,pixels,self._locked_w*SDL_BYTESPERPIXEL(pixfmt),
                   self.pixfmt,self._lockpixel,self._locked_pitch)
        else:
            assert len(pixels)==self._locked_w*self._locked_h*self.bpp,\
                'pixels size not match'
            runsdl(SDL_memcpy,self._lockpixel,pixels,self._locked_w*self._locked_h*self.bpp,
                   errval=false)
        self._lockpitch.value=self._locked_pitch

    def update(self,databuffer,pixfmt):
        assert (p:=Pref.start(name=f'{self}: update'))
        width=self.width
        height=self.height
        srcbpp=SDL_BYTESPERPIXEL(pixfmt)
        srcpitch=width*srcbpp

        data=databuffer.pixels
        databuffer.pixels=None
        assert trace(__name__,f'{self}: update start')
        with BytesIO(data) as pixels:
            data.clear()
            for y in range(height):
                with self.locker(rect=(0,y,width,1)):
                    self.stream(pixels.read(srcpitch),pixfmt)
        assert p.record(name='update')
        assert trace(__name__,f'{self}: update finished')
        assert p.log()

class TargetTexture(BaseTexture):

    __slots__=()

    def __init__(self,*args,**kwds):
        super().__init__(*args,access=SDL_TEXTUREACCESS_TARGET,**kwds)

class TextureLocker(NonWidget):

    __slots__=(
        '_texture','_rect',
    )

    def __init__(self,texture,rect):
        assert isinstance(texture,StreamTexture),\
            f'{self} only support {StreamTexture}'
        self._texture=texture
        self._rect=rect

    def __enter__(self):
        self._texture.lock(self._rect)

    def __exit__(self,exc_type,exc_value,exc_tb):
        self._texture.unlock()

class PiecesTexture(NonWidget,TextureMixIn):

    __slots__=(
        '_frameprop','_pixfmt','_renderer','_textures',
    )

    def __init__(self,renderer,pixfmt,frameprop,factory=None,**kwds):
        self._frameprop=frameprop
        self._pixfmt=pixfmt
        self._renderer=renderer
        self._textures=tuple(
            factory(renderer,pixfmt,PiecesTexture._create_pieceprop(frameprop,x,y,w,h),
                    name=f'[{n}]' if self.name is None else f'{self.name}[{n}]')
            for n,(x,y,w,h) in enumerate(
                    generate_crop_points(*frameprop.size,frameprop.blen)))
        assert trace(__name__,f'{self}: setup',bytes2str(SDLE_GetPixFmtName(pixfmt)),
                     '{}x{}'.format(*frameprop.size),len(self._textures))
        # order of textures (e.g. for R4C5):
        # +----+----+----+----+----+
        # |  0 |  4 |  8 | 12 | 16 |
        # +----+----+----+----+----+
        # |  1 |  5 |  9 | 13 | 17 |
        # +----+----+----+----+----+
        # |  2 |  6 | 10 | 14 | 18 |
        # +----+----+----+----+----+
        # |  3 |  7 | 11 | 15 | 19 |
        # +----+----+----+----+----+

    def __bool__(self):
        return all(self._textures)

    @staticmethod
    def _create_pieceprop(frameprop,x,y,w,h):
        pieceprop=frameprop.copy()
        pieceprop.blen=0
        pieceprop.size=w,h
        pieceprop.offset=map(sum,zip(pieceprop.offset,(x,y)))
        return pieceprop

    @property
    def scale_mode(self):
        return self._textures[0].scale_mode

    @scale_mode.setter
    def scale_mode(self,value):
        for texture in self._textures:
            texture.scale_mode=value
        assert trace(__name__,f'{self}: set scale mode: {value}')

    def set_blend_mode(self,blend):
        for texture in self._textures:
            texture.set_blend_mode(blend)
        self.blend=blend

    def show(self,srcrect=None,dstrect=None):
        if srcrect is None:
            srcrect=(0,0,*self.size)
        if dstrect is None:
            dstrect=(0,0,*self.renderer.size)
        srcx,srcy,srcw,srch=srcrect
        dstx,dsty,dstw,dsth=dstrect
        if not min(srcw,srch,dstw,dsth):return
        scalex=dstw/srcw
        scaley=dsth/srch
        offx=dstx
        offy=dsty
        firstoffy=None
        assert trace(__name__,f'{self}: show',
                     f'{prettyrect(srcrect)}->{prettyrect(dstrect)}')
        col=row=0
        loss_w=loss_h=0
        compensation_width=False
        for texture in self._textures:
            if srcx+srcw<=texture.offset_x or \
               srcy+srch<=texture.offset_y or \
               srcx>=texture.offset_x+texture.width or \
               srcy>=texture.offset_y+texture.height:
                # piece out of srcrect
                continue

            if firstoffy is None:
                _,firstoffy=texture.offset
            else:
                if texture.offset_y>firstoffy:
                    # add previous height to y-offset
                    row+=1
                    offy+=tgth
                else:
                    loss_h=0
                    # add previous width and offset to x-offset and reset y-offset
                    row=0
                    col+=1
                    offx+=tgtw
                    offy=dsty

            pesx=max(0,srcx-texture.offset_x)
            pesy=max(0,srcy-texture.offset_y)
            if col:
                pesw=min(srcx+srcw-texture.offset_x,texture.width)
            else:
                pesw=texture.offset_x-srcx+texture.width
            if row:
                pesh=min(srcy+srch-texture.offset_y,texture.height)
            else:
                pesh=texture.offset_y-srcy+texture.height

            tgtw,remain_w=divmod(pesw*scalex,1)
            tgth,remain_h=divmod(pesh*scaley,1)
            tgtw,tgth=map(int,(tgtw,tgth))
            assert trace(__name__,f'{self}: piece {col=} {row=}')
            loss_h+=remain_h
            if row==0:
                loss_w+=remain_w
                compensation_width=loss_w>0.999
                if compensation_width:
                    assert trace(__name__,f'{self}: compensation of 1 in width')
                    loss_w-=1
            if compensation_width:
                tgtw+=1
            if loss_h>0.999:
                assert trace(__name__,f'{self}: compensation of 1 in height')
                loss_h-=1
                tgth+=1
            texture.show(srcrect=(pesx,pesy,pesw,pesh),dstrect=(offx,offy,tgtw,tgth))

    def destroy(self):
        textures=list(self._textures)
        self._textures=None
        while textures:
            textures.pop().close()
        assert trace(__name__,f'{self}: close')

    def close(self):
        return self.destroy()

    def update(self,databuffer,pixfmt):
        pixels=databuffer.pixels
        palette=databuffer.palette
        databuffer.pixels=None
        databuffer.palette=None
        offset=0
        assert trace(__name__,f'{self}: update start')
        for (x,y,w,h),texture in zip(generate_crop_points(*self.size,self._frameprop.blen),
                                     self._textures):
            size=w*h*self.bpp
            texture.update(DataBuffer(pixels[offset:offset+size],palette),pixfmt)
            offset+=size
        assert trace(__name__,f'{self}: update finished')

def Texture(renderer,pixfmt,frameprop,access=None,name=None):
    access_types={
        'static':StaticTexture,
        'stream':StreamTexture,
    }
    assert access in access_types,f'invalid access: {access}'
    cls=access_types[access]
    if frameprop.blen:
        return PiecesTexture(renderer,pixfmt,frameprop,factory=cls,name=name)
    return cls(renderer,pixfmt,frameprop,name=name)


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
