#  This file is part of ssiv.
#
#  ssiv is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  ssiv is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with ssiv.  If not, see <https://www.gnu.org/licenses/>.

from ctypes import c_int
from os import environ

from .modules import *
from .renderer import *
from ..co import *

__all__=['Window']

class Window(Widget):

    __slots__=(
        '_pos_x','_pos_y',
        '_size_w','_size_w_max','_size_w_min',
        '_size_h','_size_h_max','_size_h_min',
        '_default_flags','_fs','_fs_flag',
        '_renderer','_renderer_driver',
        '_renderer_vsync','_renderer_drivers',
        '_window',
    )

    def __init__(self,title=None,decoration=True):
        self._pos_x=SDL_WINDOWPOS_UNDEFINED
        self._pos_y=SDL_WINDOWPOS_UNDEFINED
        self._size_w=-1
        self._size_w_max=-1
        self._size_w_min=-1
        self._size_h=-1
        self._size_h_max=-1
        self._size_h_min=-1
        self._default_flags=SDL_WINDOW_HIDDEN|SDL_WINDOW_RESIZABLE|\
            SDL_WINDOW_ALLOW_HIGHDPI
        if not decoration:
            self._default_flags|=SDL_WINDOW_BORDERLESS
        self._fs=False
        self._fs_flag=SDL_WINDOW_FULLSCREEN_DESKTOP
        self._context=runsdlP(
            SDL_CreateWindow,
            str2bytes(title),self._pos_x,self._pos_y,
            self._size_w,self._size_h,self._default_flags,
        )
        assert trace(__name__,f'{self}: setup')
        self._renderer=None
        self._renderer_driver=None
        self._renderer_vsync=False
        self._renderer_drivers=OrderedDict()

    def __enter__(self):
        return self

    def __exit__(self,exc_type,exc_value,exc_traceback):
        self.close()

    def _get_pos(self):
        runsdl(SDL_GetWindowPosition,self,x:=c_int(),y:=c_int())
        self._pos_x=x.value
        self._pos_y=y.value

    def _get_size(self):
        runsdl(SDL_GetWindowSizeInPixels,self,w:=c_int(),h:=c_int())
        self._size_w=w.value
        self._size_h=h.value

    def _get_max_size(self):
        runsdl(SDL_GetWindowMaximumSize,self,w:=c_int(),h:=c_int())
        self._size_w_max=w.value
        self._size_h_max=h.value

    def _get_min_size(self):
        runsdl(SDL_GetWindowMinimumSize,self,w:=c_int(),h:=c_int())
        self._size_w_min=w.value
        self._size_h_min=h.value

    @property
    def title(self):
        return bytes2str(runsdl(SDL_GetWindowTitle,self,errval=false))

    @title.setter
    def title(self,value):
        runsdl(SDL_SetWindowTitle,self,str2bytes(value))

    @property
    def position_x(self):
        if self._pos_x==SDL_WINDOWPOS_UNDEFINED:
            self._get_pos()
        return self._pos_x

    @position_x.setter
    def position_x(self,value):
        self._pos_x=SDL_WINDOWPOS_UNDEFINED
        runsdl(SDL_SetWindowPosition,self,value,self.position_y)

    @property
    def position_y(self):
        if self._pos_y==SDL_WINDOWPOS_UNDEFINED:
            self._get_pos()
        return self._pos_y

    @position_y.setter
    def position_y(self,value):
        self._pos_y=SDL_WINDOWPOS_UNDEFINED
        runsdl(SDL_SetWindowPosition,self,self.position_x,value)

    def move_to(self,x=SDL_WINDOWPOS_CENTERED,y=SDL_WINDOWPOS_CENTERED):
        self._pos_x=self._pos_y=SDL_WINDOWPOS_UNDEFINED
        runsdl(SDL_SetWindowPosition,self,x,y)

    @property
    def width(self):
        if self._size_w<0:
            self._get_size()
        return self._size_w

    @width.setter
    def width(self,value):
        self._size_w=-1
        runsdl(SDL_SetWindowSize,self,value,self.height)

    @property
    def height(self):
        if self._size_h<0:
            self._get_size()
        return self._size_h

    @height.setter
    def height(self,value):
        self._size_h=-1
        runsdl(SDL_SetWindowSize,self,self.width,value)

    def resize(self,w,h):
        self._size_w=self._size_h=-1
        runsdl(SDL_SetWindowSize,self,w,h)

    @property
    def max_width(self):
        if self._size_w_max<0:
            self._get_max_size()
        return self._size_w_max

    @width.setter
    def max_width(self,value):
        self._size_w_max=-1
        runsdl(SDL_SetWindowMaximumSize,self,value,self.max_height)

    @property
    def max_height(self):
        if self._size_h_max<0:
            self._get_max_size()
        return self._size_h_max

    @height.setter
    def max_height(self,value):
        self._size_h_max=-1
        runsdl(SDL_SetWindowMaximumSize,self,self.max_width,value)

    @property
    def min_width(self):
        if self._size_w_min<0:
            self._get_min_size()
        return self._size_w_min

    @width.setter
    def min_width(self,value):
        self._size_w_min=-1
        runsdl(SDL_SetWindowMinimumSize,self,value,self.min_height)

    @property
    def min_height(self):
        if self._size_h_min<0:
            self._get_min_size()
        return self._size_h_min

    @height.setter
    def min_height(self,value):
        self._size_h_min=-1
        runsdl(SDL_SetWindowMinimumSize,self,self.min_width,value)

    @property
    def fullscreen(self):
        return self._fs

    @fullscreen.setter
    def fullscreen(self,value):
        status=not not self.fullscreen
        if (not not value)==status:return
        self._fs=not status
        runsdl(SDL_SetWindowFullscreen,self,0 if status else self._fs_flag)

    @property
    def fullscreen_change_videomode(self):
        return self._fs_flag==SDL_WINDOW_FULLSCREEN

    @fullscreen_change_videomode.setter
    def fullscreen_change_videomode(self,value):
        self._fs_flag=SDL_WINDOW_FULLSCREEN_DESKTOP \
            if not value else SDL_WINDOW_FULLSCREEN

    @property
    def screensaver(self):
        return runsdl(SDL_IsScreenSaverEnabled,errval=false)==SDL_TRUE

    @screensaver.setter
    def screensaver(self,value):
        runsdl(SDL_EnableScreenSaver if value else SDL_DisableScreenSaver)

    def new_renderer(self):
        index,flags=self.renderer_driver_property(self.renderer_driver)
        flags&=SDL_RENDERER_ACCELERATED|SDL_RENDERER_SOFTWARE|SDL_RENDERER_TARGETTEXTURE
        if self.renderer_vsync:flags|=SDL_RENDERER_PRESENTVSYNC
        assert trace(__name__,f'{self}: create new renderer')
        return runsdlP(SDL_CreateRenderer,self,index,flags)

    @property
    def renderer_drivers(self):
        return tuple(self._renderer_drivers)

    @property
    def renderer(self):
        if self._renderer is None:
            self._renderer=Renderer(self)
        return self._renderer

    @property
    def renderer_driver(self):
        if self._renderer is None:
            if self._renderer_driver is None:
                self._renderer_driver=self.renderer_drivers[0]
            return self._renderer_driver
        return self._renderer.driver

    @renderer_driver.setter
    def renderer_driver(self,value):
        if value==self.renderer_driver:return
        assert value in self.renderer_drivers
        self._renderer_driver=value
        if self._renderer is None:return
        self._renderer.setup(self)

    def renderer_driver_property(self,value):
        return self._renderer_drivers[value]

    @property
    def renderer_vsync(self):
        if self._renderer is None:
            return self._renderer_vsync
        return self._renderer.vsync

    @renderer_vsync.setter
    def renderer_vsync(self,value):
        if (value:=not not value)==self.renderer_vsync:return
        if self.renderer_driver=='software' and not value:
            raise ValueError('software renderer always enable vsync.')
        self._renderer_vsync=value
        if self._renderer is None:return
        self._renderer.setup(self)

    @property
    def renderer_drivers(self):
        if not self._renderer_drivers:
            info=SDL_RendererInfo()
            for n in range(runsdl(SDL_GetNumRenderDrivers,errval=falseuint)):
                runsdl(SDL_GetRenderDriverInfo,n,info)
                self._renderer_drivers[bytes2str(info.name)]=(n,info.flags)
        return tuple(self._renderer_drivers)

    @property
    def available_window_drivers(self):
        yield 'available window drivers:'
        for n in range(runsdl(SDL_GetNumVideoDrivers,errval=falseuint)):
            yield f'{n}:{bytes2str(runsdl(SDL_GetVideoDriver,n,errval=false))}'

    @property
    def window_driver(self):
        return bytes2str(runsdl(SDL_GetCurrentVideoDriver,errval=false))

    @property
    def available_renderer_drivers(self):
        yield 'available renderer drivers and flags:'
        for driver in self.renderer_drivers:
            n,flags=self.renderer_driver_property(driver)
            yield f'{n}:{driver}'
            yield from (f'\t{flagname}' for flagname in (
                'SDL_RENDERER_SOFTWARE',
                'SDL_RENDERER_ACCELERATED',
                'SDL_RENDERER_PRESENTVSYNC',
                'SDL_RENDERER_TARGETTEXTURE',
            ) if flags&globals()[flagname])

    def show(self):
        runsdl(SDL_ShowWindow,self)
        assert trace(__name__,f'{self}: show')

    def hide(self):
        runsdl(SDL_HideWindow,self)
        assert trace(__name__,f'{self}: hide')

    def raise_window(self):
        runsdl(SDL_RaiseWindow,self)
        assert trace(__name__,f'{self}: raise')

    def update(self):
        self.renderer.size=self.renderer.output_size
        assert trace(__name__,f'{self}: update')

    def destroy(self):
        if self._renderer is not None:
            self._renderer.close()
        if self._context is not None:
            assert trace(__name__,f'{self}: close')
            window=self._context
            self._context=None
            runsdl(SDL_DestroyWindow,window)

    def close(self):
        return self.destroy()

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
